# Challenge 2014 - Browser Wars

2014 台大椰林資訊營 python AI Challenge 小遊戲

# 系統需求
* python3
* pygame

# 使用方法
`python3 main.py [TEAM_NUM [TEAM_NUM [TEAM_NUM [TEAM_NUM]]]]`  
輸入 AI 編號即可以指定 AI 操控角色，留空則以左上數字 1~4 手動控制

# 開發人員
* 程式組：
    * 組長：林天翼
    * 遊戲組：楊子由、曹爗文、張志宏
    * 物理組：陳璽安、邱筱晴、李彥霆
    * 畫面組：柯東爵、黃宇、陳聖曄

* 美工組：
    * 畫面：藍祥予、宋品賢
    * 音效：張志宏、林天翼

* 企劃/文件組：
    * 張志宏、林品君、蔡昆翰