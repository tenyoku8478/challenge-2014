#!/usr/local/bin/python3

import pygame

from pygame import display
from pygame import draw
from pygame import event
from pygame import font
from pygame import image
from pygame import mouse
from pygame import transform
from pygame.locals import *
class Score_Board :
    
    def __init__(self,screen,teams,browers):
        self.screen = screen
        self.teamfont = font.SysFont('helvetica', 36)
        self.teams=teams   # modified this line at last
        self.browers=browers
        self.ten = [0,10,0,10,0]
        self.num = []
        fname = "./browsers/num/0-1.png"
        for j in range(2) :
            fname=fname[0:17]+str(j+1)+fname[18:];
            for i in range(10) :
                fname=fname[0:15]+str(i)+fname[16:];
                tmp=pygame.image.load(fname)
                w,h=tmp.get_size()
                tmp=transform.scale(tmp,((w//2),(h//2)))
                self.num.append(tmp)
    
    def change_ten(self) :
        if self.ten[0]==0 :
            self.ten=[10,0,10,0,10]
        else :
            self.ten=[0,10,0,10,0]            
    
    def print_team(self,x,y,score,team,brower) :
        team_name=self.teamfont.render(team, 1, (0, 0, 0))
        self.screen.blit(team_name,(x,y))
        self.screen.blit(brower, (x+230,y-10))
        dig = 10000
        for i in range(5) :
            t = score//dig
            score%=dig
            dig//=10
            self.screen.blit(self.num[ (t+self.ten[i]) ], (x,y+32))
            x+=45
    
    def print_board(self) :
        for i in range(4) :
            self.print_team(800,(30+100*i),0,self.teams[i],self.browers[i])

def main():
    pygame.init()
    pygame.font.init()
    screen = pygame.display.set_mode((1200, 600), 0, 32)
    pygame.display.set_caption("Scoreboard")
    IE=pygame.image.load("IE.png")
    w,h=IE.get_size()
    IE=transform.scale(IE,((w//3),(h//3)))
    browers=[IE,IE,IE,IE]
    teams=["team one","team two","team three","team four"]
    scoreboard=Score_Board(screen,teams,browers)
    count=0
    while(True):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
        screen.fill((255,255,255))
        count+=1
        if count%50==0 :
            scoreboard.change_ten()
        scoreboard.print_board()
        screen.blit(IE, (0,0))
        pygame.display.update()	
if __name__ == "__main__":
    main()
