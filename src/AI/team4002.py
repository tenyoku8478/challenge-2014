'''
    Swap no player 
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

SKILL = {
    "Swap": 100000, # 來 你來 來
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "SwapNone"

    def decide(self, helper):
        return ActionSkillSwap
