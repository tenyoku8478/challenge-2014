'''
    Excess use skill 
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

LIST = [
    ActionSkillSwap,
    ActionSkillAbs,
    ActionSkillCold,
    ActionSkillInvisible,
    ActionSkillChaseBeat,
    ActionSkillScoreDouble,
    ActionSkillPower
]

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "Excskill"
        self.cnt = 0
        
    def decide(self, helper):
        self.cnt = (self.cnt + 1)%7
        return LIST[self.cnt]
