import math
from AI.base_ai import BaseAi
from AI.ai_config import *

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "鎖定人撞"
        self.target = None

    def decide(self, helper):
        if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
            self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻

        if helper.me.check_facing(self.target.get_position()):
            return ActionPress
        else:
            return ActionUnPress
