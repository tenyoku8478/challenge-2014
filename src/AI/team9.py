'''
    AI template
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0, # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "拉拉兔" #這是一個bj4的概念
        self.target = None
        self.click = False
        self.a = 1 #霸王色
        self.b = 1 #加倍奉還
        self.c = 1 #沒使用過加倍奉還
        self.d = 1 #你有聽過lag嗎
        self.e = 1 #進擊的巨人
        self.f = 1 #隱形
        self.g = 1 #交換位置
       
    def decide(self, helper):
        if self.a > 0 :
            self.a = 0
            return ActionSkillPower  # 使用 霸王色霸氣
        T = helper.get_remain_time()
        if T == 90 :
            if self.b > 0 :
                self.b = 0
                self.c = 0
                return ActionSkillScoreDouble #使用 加倍奉還
        if self.c == 0 :
            self.c = 1
            return ActionSkillChaseBeat #使用 香蕉
        if T == 30 :
            if self.d > 0 :
                self.d = 0
                self.e = 0
                return ActionSkillCold #使用 lag
        if self.e == 0 :
            self.e = 1
            return ActionSkillAbs #使用 巨人
        if T == 15 :
            if self.f > 0 :
                self.f = 0
                return ActionSkillInvisible #使用隱形
        nowx, nowy = helper.me.get_position()
        dist = math.sqrt(nowx * nowx + nowy * nowy)
        if dist >= (80.0 * helper.get_field_radius() / 100.0) and not helper.me.get_skill_count('Swap') == 0:
            sa = helper.get_others_distance()
            ff = False
            ansx = 0
            ansy = 0
            pl = None
            for cc in sa:
                tttx, ttty = cc.get_position()
                if not ff:
                    ff = True
                    ansx = tttx
                    ansy = ttty
                    pl = cc
                elif tttx * tttx + ttty * ttty < ansx * ansx + ansy *ansy:
                    ansx = tttx
                    ansy = ttty
                    pl = cc
                    
            return ActionSkillSwap, pl
        #距離圓心>=(95/100)半徑 則使用{來 你來}這張卡 與距離圓心最近的對手交換
           #這裡還沒有寫好  
            
        R = helper.get_field_radius()
        if self.click:
            self.click = False
            return ActionUnPress
        
        if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
            ttttt = helper.get_others_degree()
            if not ttttt == []:
                self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻

        if helper.me.check_facing(self.target.get_position(),(5*3.1415926)/180):
            return ActionPress
        else:
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                self.click = True # 輕按一下等一下馬上放掉
                return ActionPress
            return ActionUnPress
        cards = helper.get_cards()
        for card in cards:
            if helper.me.check_facing(card, (5*3.1415926)/180):
                return ActionPress
        
        k = helper.me.get_position_r()
        if	(16*R)/20 <= k[0]:
            theta = (helper.me.get_position_r()[1] + 3.1415926) % 6.28
            while k[0]<=2*R/5 :
                if(theta-0.05<k[1]<theta+0.05):
                    return ActionPress
                else:
                    while not (theta-0.1 < helper.me.degree() < theta+0.1):
                        return AcitonUnPress
        
