'''
    team1 ai test
'''
import math
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "傻逼2"
        self.ESP = 0.1
        self.cnt = 0

    def decide(self, helper):
        other = helper.get_others_degree()[0]
        if helper.me.check_facing(other.get_position(), self.ESP):
            self.ESP = 0.2
            self.cnt = 10
            return AI.ai_config.ActionPress
        else:
            if self.cnt > 0:
                self.cnt -= 1;
                return AI.ai_config.ActionPress
            else:
                self.ESP = 0.1
                return AI.ai_config.ActionUnPress
