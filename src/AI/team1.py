import math
from AI.base_ai import BaseAi
from AI.ai_config import *
# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "泰瑞一小"
        self.target = None
        self.click = False
        self.swap = 200
        self.power = 200
        self.Abs = 200
        self.lag = True
        self.banana = True
        self.invisible = 200
    def decide(self, helper):
        if self.click:
            self.click = False
            return ActionUnPress
        #攻擊技能:自己為處邊緣時對較靠近中心的敵人使用"來你來來"
        radius = helper.get_field_radius()
        time = helper.get_remain_time()
        if helper.me.get_position_r()[0] > (radius * 4 / 5) and helper.me.get_speed()[0] < 10 and self.swap - time>=10 and SKILL["Swap"]!=0 :
            tar = None
            for x in helper.get_others_distance() :
                if x.is_dead() :
                    continue
                if tar == None :
                    tar = x
                elif tar.get_position_r()[0]>x.get_position_r()[0] :
                    tar = x
            if tar != None :
                self.swap = time
                return ActionSkillSwap, tar
        #防守技能:兩個以上敵人同時為處邊緣時使用"霸王色霸氣"
        danger = 0
        for x in helper.get_others_degree() :
                if x.get_position_r()[0] > (radius * 4 / 5) :
                    danger+=1
        if danger>=2 and self.power  - time>=10 and SKILL["Power"]!=0 :
            self.power = time
            return ActionSkillPower
        #攻擊技能:兩個敵人靠近自己時使用"晉級的巨人"
        near = 0
        for x in helper.get_others_degree() :
                if x.cal_vector(helper.me.get_position())[0] <= 100 :
                    near+=1
        if near>=2 and self.Abs  - time>=10 and SKILL["Abs"]!=0 :
            self.Abs = time
            return ActionSkillAbs
        #防守:時間過一半時使用"LAG"
        if self.lag and time == 60 and SKILL["Cold"]!=0 :
            self.lag = False
            return ActionSkillCold
        #別人質量變大時使用"你看不到我"
        big = False
        for x in helper.get_others_degree() :
            if x.get_mass()==6 :
                big = True
        if big and  self.invisible - time>=15 and SKILL["Invisible"]!=0:
            self.invisible = time
            return ActionSkillInvisible
        #時間過2/3時使用"一大堆香蕉"
        if self.banana and time == 40 and SKILL["ChaseBeat"]!=0:
            self.banana = False
            return ActionSkillChaseBeat

        if self.target == None or self.target.is_dead() :# 如果沒有目標或目標已死
            self.target = None
            for x in helper.get_others_degree() :
                if x.get_position_r()[0] > (radius * 4 / 5) :
                    self.target = x #最靠近自己
            if self.target == None :
                if helper.get_others_distance() and not helper.get_others_distance()[0].is_dead() :
                    self.target = helper.get_others_distance()[0]
        if self.target == None :
            return ActionUnPress
        if helper.me.cal_vector(self.target.get_position())[0] > radius*1.5 :
            devia = math.pi/10
        elif helper.me.cal_vector(self.target.get_position())[0] > radius*0.5 :
            devia = math.pi/20
        else : 
            devia = math.pi/26
        if helper.me.check_facing(self.target.get_position(),devia) :#誤差值有問題
            return ActionPress
        else:
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                self.click = True # 輕按一下等一下馬上放掉
                return ActionPress
            return ActionUnPress
       
