'''
    skill cold AI demo
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

SKILL = {"Cold" : 1}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "Cold"

    def decide(self, helper):
        # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if not helper.others or helper.me.get_skill_count('Cold') == 0:
            return ActionUnPress
        else: 
            return ActionSkillCold
