'''
    hh ai test
'''
import math
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "hh"
        self.EPS = 0.2
        self.click = False

    def decide(self, helper):
        if self.click:
            self.EPS = 0.2
            self.click = False
            return AI.ai_config.ActionUnPress
        cards = helper.get_cards()
        if len(cards) > 0:
            if helper.me.check_facing(cards[0], self.EPS):
                self.EPS = 0.4
                return AI.ai_config.ActionPress
            else:
                if helper.me.cal_rel_degree(cards[0]) > 2*math.pi - helper.me.cal_rel_degree(cards[0]) + self.EPS:
                    self.click = True
                    return AI.ai_config.ActionPress
            self.EPS = 0.2
            return AI.ai_config.ActionUnPress    
        attack_list = helper.get_others_degree()
        if len(attack_list) == 0:
            if helper.me.check_facing((0,0), self.EPS):
                self.EPS = 0.4
                return AI.ai_config.ActionPress
            else:
                if helper.me.cal_rel_degree((0,0)) > 2*math.pi - helper.me.cal_rel_degree((0,0)) + self.EPS:
                    self.click = True
                    return AI.ai_config.ActionPress
                return AI.ai_config.ActionUnPress
            self.EPS = 0.2
            return AI.ai_config.ActionUnPress
        if helper.me.check_facing(attack_list[0].get_position(), self.EPS):
            self.EPS = 0.4
            return AI.ai_config.ActionPress
        else:
            if helper.me.cal_rel_degree(attack_list[0].get_position()) > 2*math.pi - helper.me.cal_rel_degree(attack_list[-1].get_position()) + self.EPS:
                self.click = True
                return AI.ai_config.ActionPress
        self.EPS = 0.2
        return AI.ai_config.ActionUnPress
