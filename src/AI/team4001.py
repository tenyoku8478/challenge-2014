'''
    None Return Value Test
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "No Ret"

    def decide(self, helper):
        return
