import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0,#你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 #加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "eight" # 隊伍名稱
        self.target=None
        self.click=False
        self.invisible_used=2147483647
        self.power_used  = 2147483647
        self.abs_used = 2147483647
        self.ChaseBeat_used = 2147483647
        self.cold_used = 2147483647
        self.Swap_used = 2147483647
        self.ScoreDouble_used = 2147483647
        # 這裡可以放變數暫存一些資訊

    def decide(self, helper):
        # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        
        if  helper.others and helper.me.get_skill_count('Invisible') != 0 and self.invisible_used - helper.get_remain_time() > 15:
            self.invisible_used = helper.get_remain_time()
            return ActionSkillInvisible
        
        
        # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if  helper.others and helper.me.get_skill_count('Swap') != 0 and self.Swap_used - helper.get_remain_time() > 15:
            self.Swap_used = helper.get_remain_time()
            return ActionSkillSwap, helper.others[0]
            
        
        if self.click:
            self.click = False
            return ActionUnPress
        
        # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if  helper.others and helper.me.get_skill_count('Power') != 0 and self.power_used - helper.get_remain_time() > 5:
            self.power_used = helper.get_remain_time()
            return ActionSkillPower

            
             # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if  helper.others and helper.me.get_skill_count('Abs') != 0 and self.abs_used - helper.get_remain_time() > 10:
            self.abs_used = helper.get_remain_time()
            return ActionSkillAbs
            
        
            
            # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if  helper.others and helper.me.get_skill_count('ChaseBeat') != 0 and self.ChaseBeat_used - helper.get_remain_time() > 5:
            self.ChaseBeat_used = helper.get_remain_time()
            return ActionSkillChaseBeat

        if  helper.others and helper.me.get_skill_count('Cold') != 0 and self.cold_used - helper.get_remain_time() > 10:
            self.cold_used = helper.get_remain_time()
            return ActionSkillCold
            
        if  helper.others and helper.me.get_skill_count('ScoreDouble') != 0 and self.ScoreDouble_used - helper.get_remain_time() > 15:
            self.ScoreDouble_used = helper.get_remain_time()
            return ActionSkillScoreDouble

        
        if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
            if helper.get_others_degree():
                self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻

        if helper.me.check_facing(self.target.get_position(),0.3):
            return ActionPress
        else:
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                self.click = True # 輕按一下等一下馬上放掉
                return ActionPress
            return ActionUnPress
            
        # 以下沒屁用zzz
        # 以上是輕按反轉的功能，上下未合併
            
        cards = helper.get_cards()
        for card in cards:
            if helper.me.check_facing(card):
            
                if helper.get_chance():
                    if helper.me.check_facing(card.get_position(),0.3):
                        return ActionPress
                    
                                
                    '''temp=helper.get_others_degree() [0].get_position
                    temp1=helper.get_others_degree() [1].get_position
                    temp2=helper.get_others_degree() [2].get_position
                    
                    a=0
                    
                    if helper.get_others_degree() [0].get_position == temp
                        a=a+1
                    if helper.get_others_degree() [1].get_position == temp1
                        a=a+1
                    if helper.get_others_degree() [2].get_position == temp2
                        a=a+1
                    while a>1 
                        self.target=helper.get_others_degree() [0]'''
                    '''
                    if degree[0].speed==0 or [1]==0 or [2]==0
                        
                    else:
                        degre[0]
                    '''
                    
                    #先檢查全部是否為靜止，若有靜止先撞角度小的，若無，則撞角度小的
                    if helper.get_others_degree() [0].get_speed()==0 or helper.get_others_degree() [1].get_speed()==0 or helper.get_others_degree() [2].get_speed()==0:
                    
                        if helper.get_others_degree() [0].get_speed()==0:
                            return ActionPress
                        elif helper.get_others_degree() [1].get_speed()==0:
                            return ActionPress
                        elif helper.get_others_degree() [2].get_speed()==0:
                            return ActionPress
                    
                            '''if self.target!=None or self.target.is_dead:
                                if helper.me.check_facing(self.target.get_position(),0.3):
                                    return ActionPress'''
                    else:
                        if helper.me.check_facing(helper.get_others_degree() [0]):
                            return ActionPress
                        else:
                            return ActionUnPress
                    
                    
                '''檢查是否暈眩或靜止
                while self.target!=None or self.target.is_dead
                if player.cal_vector(helper.cal_vector(helper.get_chance()))< 2/3*helper.get_field_radius():
                        if player.cal_vector(helper.cal_vector(helper.get_chance()))<self.target.get_distance
                            helper.me.check_facing(other.get_position()):
                            return ActionPress
                        else helper.me.check_facing(other.get_position()):
                
                    
                    
                    
                    
                    
                    return ActionPress
        return ActionUnPress'''
        
        
        
        '''
        '''
            
            
            
            
            
            
            
        
