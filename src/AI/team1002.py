'''
    失敗品
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

import Config.event_config as EVENT_CON
import Config.game_config as GAME_CON
import Config.env_config as ENV_CON

import math
import random

class Line:
    def __init__(self, a=0, b=0, c=0):
        self.a = 0
        self.b = 0
        self.c = 0

    def set_by_vector_point(self, vec, pt):
        self.a = vec.x
        self.b = vec.y
        self.c = (-1)*(self.a*pt.x + self.b*pt.y)
        if self.a <= 0 and self.b <= 0 and self.c <= 0:
            self.a = -self.a
            self.b = -self.b
            self.c = -self.c

    def set_by_point_point(self, pt1, pt2):
        self.a = pt2.y - pt1.y
        self.b = -(pt2.x - pt1.x)
        self.c = (-1)*(self.a*pt1.x + self.b*pt1.y)
        if self.a <= 0 and self.b <= 0 and self.c <= 0:
            self.a = -self.a
            self.b = -self.b
            self.c = -self.c


class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def set_by_tuple(self, tup):
        self.x = tup[0]
        self.y = tup[1]


class Vector:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def set_by_xy(self, x, y):
        self.x = x
        self.y = y

    def set_by_rtheta(self, r, theta):
        self.x = r*math.cos(theta)
        self.y = r*math.sin(theta)

    def get_len(self):
        return math.sqrt(self.x**2 + self.y**2)

    def set_len(self, l):
        self.set_normal()
        self.x *= l
        self.y *= l

    def set_normal(self):
        ll = self.get_len()
        self.x = self.x / ll
        self.y = self.y / ll

    def get_degree(self):
        import math
        arc = math.atan2(self.y, self.x)
        if arc < 0:
            arc += 2*math.pi
        return arc


class MathTool:
    inst = None
    def __init__(self):
        inst = self

    def get_dist_line_point(self, line, pt):
        return (line.a*pt.x + line.b*pt.y + line.c)/math.sqrt(line.a**2 + line.b**2)

    def get_dist_point_point(self, pt1, pt2):
        return math.sqrt((pt1.x - pt2.x)**2 + (pt1.y - pt2.y)**2)

    def get_a_mod_b(self, a, b):
        while(a < 0):
            a += b
        while(a >= b):
            a -= b
        return a

def make_point_by_tuple(tup):
    p = Point()
    p.set_by_tuple(tup)
    return p

STATUS_NONE = 0
STATUS_RETURN = 1
STATUS_BATTLE = 2
STATUS_CARD = 3
STATUS_PUSH = 4

EPS1 = 0.1
EPS1_1 = 0.2
EPS2 = 0.3
EPS3 = 0.5
EPS_LEN = 10

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "失敗品"

        self.env = env
        self.status = (STATUS_BATTLE, None)
        self.clickstatus = 0
        self.eps = EPS1
        self.mt = MathTool()
    
    def get_dist_edge(self, helper):
        p1 = Point()
        p2 = Point()
        p1.set_by_tuple(ENV_CON.ROOM_CTR)
        p2.set_by_tuple(helper.me.player.char.get_center())
        det = ENV_CON.ROOM_RADIUS - self.mt.get_dist_point_point(p1, p2)
        return det

    def get_dist_center(self, helper):
        p1 = Point()
        p2 = Point()
        p1.set_by_tuple(ENV_CON.ROOM_CTR)
        p2.set_by_tuple(helper.me.player.char.get_center())
        det = self.mt.get_dist_point_point(p1, p2)
        return det

    def get_player_arc(self, player):
        vv = Vector(player.char.position['x'] - ENV_CON.ROOM_CTR[0], player.char.position['y'] - ENV_CON.ROOM_CTR[1])
        return vv.get_degree()

    def check_way_clear(self, helper, target_point):
        playerarea = 0
        ll = Line()
        p = Point()
        p.set_by_tuple(helper.me.player.char.get_center())
        ll.set_by_point_point(target_point, p)
        for other_player in self.env["live_player"]:
            if other_player is helper.me.player: continue
            p1 = Point()
            p1.set_by_tuple(other_player.char.get_center())
            if self.mt.get_dist_line_point(ll, p1) <= ENV_CON.PLAYER_SIZES[1]:
                playerarea += other_player.char.radius**2

        if playerarea/(ENV_CON.PLAYER_SIZES[1]*self.mt.get_dist_point_point(target_point, p)) >= 0.5:
            return False
        else:
            return True

    def check_point_clear(self, helper, target_point):
        playerarea = 0
        for other_player in self.env["live_player"]:
            if other_player is helper.me.player: continue
            p1 = Point()
            p1.set_by_tuple(other_player.char.get_center())
            if self.mt.get_dist_point_point(p1, target_point) <= ENV_CON.PLAYER_SIZES[1]:
                playerarea += other_player.char.radius**2

        if playerarea/(ENV_CON.PLAYER_SIZES[1]**2) >= 0.5:
            return False
        else:
            return True

    def decide(self, helper):

        # CLICK STATUS
        if self.clickstatus == 1:
            self.clickstatus = 2
            return ActionUnPress
        elif self.clickstatus == 2:
            self.clickstatus = 3
            return ActionPress
        elif self.clickstatus == 3:
            self.clickstatus = 0
            return ActionUnPress


        for other_player in self.env["player"]:
            if other_player is helper.me.player : continue
            if math.fabs(self.get_player_arc(other_player) - self.get_player_arc(helper.me.player)) <= EPS1 :
                len_tar = self.mt.get_dist_point_point(
                    make_point_by_tuple(other_player.char.get_center()),
                    make_point_by_tuple(ENV_CON.ROOM_CTR)
                )
                len_me = self.mt.get_dist_point_point(
                    make_point_by_tuple(helper.me.player.char.get_center()),
                    make_point_by_tuple(ENV_CON.ROOM_CTR)
                )
                if len_tar > ENV_CON.ROOM_RADIUS*3/4 :
                    if math.fabs(self.get_player_arc(other_player) - helper.me.player.char.degree) <= EPS1:
                        print("Catch!")
                        self.status = (STATUS_BATTLE, other_player)
                        return ActionPress

        if self.status[0] == STATUS_NONE:
            if self.get_dist_edge(helper) <= ENV_CON.ROOM_RADIUS/5:
                self.eps = EPS1
                self.status = (STATUS_RETURN, None)
            else:
                if len(self.env["cards"]) > 0:
                    self.status = (STATUS_CARD, None)
                else:
                    self.eps = EPS1
                    self.status = (STATUS_BATTLE, None)
            return ActionUnPress
        elif self.status[0] == STATUS_PUSH:
            print("status -> PUSH")
            if self.status[1] not in self.env["live_player"]:
                self.status = (STATUS_NONE, None)
                return ActionUnPress
            else:
                # check radius to center
                len_tar = self.mt.get_dist_point_point(
                    make_point_by_tuple(self.status[1].char.get_center()),
                    make_point_by_tuple(ENV_CON.ROOM_CTR)
                )
                len_me = self.mt.get_dist_point_point(
                    make_point_by_tuple(helper.me.player.char.get_center()),
                    make_point_by_tuple(ENV_CON.ROOM_CTR)
                )
                if math.fabs(self.get_player_arc(self.status[1]) - self.get_player_arc(helper.me.player)) <= EPS1 :
                    if len_tar < len_me :
                        # MAY USE STATUS_RETURN
                        print("trigger : PUSH -> status resume")
                        self.status = (STATUS_NONE, None)
                        return ActionUnPress

                    # type1: stra line:
                    print("trigger : PUSH -> type::stra_line")
                    return ActionPress

                if len_tar > len_me:
                    print("trigger : PUSH -> type::line")
                    # type1: stra line:
                    return ActionPress
                else:
                    print("trigger : PUSH -> type::smooth")
                    # type2: smooth push:
                    xx, yy = self.status[1].char.get_center()
                    vv = Vector(ENV_CON.ROOM_CTR[0] - xx, ENV_CON.ROOM_CTR[1] - yy)
                    vv.set_len(self.status[1].char.radius + 20)
                    x1, y1 = xx + vv.x, yy + vv.y
                    x2, y2 = helper.me.player.char.get_center()
                    tar_deg = Vector(x1 - x2, y1 - y2).get_degree()
                    now_deg = helper.me.player.char.degree
                    now_dir = helper.me.player.char.rotate_dir
                    if math.fabs(tar_deg - now_deg) <= self.eps:
                        self.eps = EPS3
                        return ActionPress
                    elif tar_deg > now_deg and now_dir == -1:
                        if helper.me.player.char.pressed:
                            self.clickstatus = 2
                            return ActionUnPress
                        else:
                            self.clickstatus = 3
                            return ActionPress
                    elif tar_deg < now_deg and now_dir == 1:
                        if helper.me.player.char.pressed:
                            self.clickstatus = 2
                            return ActionUnPress
                        else:
                            self.clickstatus = 3
                            return ActionPress

                self.status = (STATUS_NONE, None)
                return ActionUnPress

                # check type

        elif self.status[0] == STATUS_BATTLE:
            if self.status[1] == None:
                # Find one player
                if len(self.env["live_player"]) <= 1:
                    self.status = (STATUS_RETURN, None)
                else:
                    # MAKE MORE stragty
                    choose_list = []
                    for other_player in self.env["live_player"]:
                        if other_player is helper.me.player: continue
                        if other_player.char.radius > helper.me.player.char.radius: continue
                        if other_player.char.speed > helper.me.player.char.speed: continue
                        pt = make_point_by_tuple(other_player.char.get_center())
                        cent = make_point_by_tuple(ENV_CON.ROOM_CTR)
                        if self.check_way_clear(helper, pt):
                            choose_list.append(other_player)
                            continue

                        if  ENV_CON.ROOM_RADIUS - self.mt.get_dist_point_point(pt, cent) < 40:
                            choose_list.append(other_player)
                            continue
                    if len(choose_list) == 0:
                        self.status = (STATUS_RETURN, None)
                    else:
                        self.status = (STATUS_BATTLE, random.choice(choose_list))
                return ActionUnPress
            else:
                # check if the target is alive
                if self.status[1] not in self.env["live_player"]:
                    print("change")
                    self.status = (STATUS_NONE, None)
                    return ActionUnPress
                
                x1, y1 = self.status[1].char.get_center()
                x2, y2 = helper.me.player.char.get_center()
                tar_deg = Vector(x1 - x2, y1 - y2).get_degree()
                if (Vector(x1 - x2, y1 - y2).get_len() <= self.status[1].char.radius + helper.me.player.char.radius + EPS_LEN) and (helper.me.player.char.speed <= 5) :
                    self.status = (STATUS_PUSH, self.status[1])
                    return ActionPress

                now_deg = helper.me.player.char.degree
                now_dir = helper.me.player.char.rotate_dir

                if math.fabs(tar_deg - now_deg) <= self.eps:
                    self.eps = EPS2
                    return ActionPress

                elif tar_deg > now_deg and now_dir == -1:
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress

                elif tar_deg < now_deg and now_dir == 1:
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress

                return ActionUnPress

        elif self.status[0] == STATUS_RETURN:
            cent = Point()
            cent.set_by_tuple(ENV_CON.ROOM_CTR)
            if self.get_dist_center(helper) <= ENV_CON.ROOM_RADIUS/4:
                self.status = (STATUS_NONE, None)
                return ActionUnPress
            elif self.check_way_clear(helper, cent) == False or self.check_point_clear(helper, cent) == False:
                print("center not clear")
                self.status = (STATUS_BATTLE, None)
                return ActionUnPress
            else:
                x1, y1 = ENV_CON.ROOM_CTR
                x2, y2 = helper.me.player.char.get_center()
                tar_deg = Vector(x1 - x2, y1 - y2).get_degree()
                now_deg = helper.me.player.char.degree
                now_dir = helper.me.player.char.rotate_dir
                if math.fabs(tar_deg - now_deg) <= self.eps:
                    self.eps = EPS2
                    return ActionPress
                elif tar_deg > now_deg and now_dir == -1: 
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress
                elif tar_deg < now_deg and now_dir == 1:
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress

                return ActionUnPress

            return ActionUnPress
        elif self.status[0] == STATUS_CARD:
            if self.status[1] == None:
                # Choose a card
                if len(self.env["cards"]) == 0:
                    self.status = (STATUS_NONE, None)
                    return ActionUnPress

                self.status = (STATUS_CARD, random.choice(self.env["cards"]))
                return ActionUnPress

            else:
                # Try to eat card
                if self.status[1] not in self.env["cards"]:
                    self.status = (STATUS_NONE, None)
                    return ActionUnPress
                x1, y1 = self.status[1].get_center()
                x2, y2 = helper.me.player.char.get_center()
                tar_deg = Vector(x1 - x2, y1 - y2).get_degree()
                now_deg = helper.me.player.char.degree
                now_dir = helper.me.player.char.rotate_dir

                vline = Line()
                vv = Vector()
                vv.set_by_rtheta(1, now_deg)
                vline.set_by_vector_point(vv, Point(x2, y2))

                if self.mt.get_dist_line_point(vline, Point(x1, y1)) <= helper.me.player.char.radius:
                    self.eps = EPS2
                    return ActionPress
                elif tar_deg > now_deg and now_dir == -1:
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress
                elif tar_deg < now_deg and now_dir == 1:
                    if helper.me.player.char.pressed:
                        self.clickstatus = 2
                        return ActionUnPress
                    else:
                        self.clickstatus = 3
                        return ActionPress

                return ActionUnPress

            return ActionUnPress
