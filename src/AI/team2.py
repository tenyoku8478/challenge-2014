﻿import math
from AI.base_ai import BaseAi
from AI.ai_config import *

SKILL = {
    "Invisible" : 2, # 你看不到我
    "Swap": 1, # 來 你來 來
    "Power": 2, # 霸王色霸氣
    "Abs": 2, # 進擊的巨人
    "ChaseBeat": 3, # 一大堆香蕉
    "Cold": 1, # 你聽過 Lag 嗎
    "ScoreDouble": 2 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "小小兵２"
        self.target = None
        self.click = False
        self.back = False
        self.ScoreDouble = 0
        self.ChaseBeat = 0
        self.Abs = 0
        self.Cold = 0
        self.Power = 0
        self.Invisible = 0
    def decide(self, helper):
        
        if helper.get_remain_time() == 114:
            if self.ScoreDouble == 0:
                self.ScoreDouble = 1
                return ActionSkillScoreDouble
        if helper.get_remain_time() == 113:
            if self.ChaseBeat == 0:
                self.ChaseBeat = 1
                return ActionSkillChaseBeat
        if helper.get_remain_time() == 112:
            if self.Abs == 0:
                self.Abs = 1
                return ActionSkillAbs
        if helper.get_remain_time() == 111:
            if self.Cold == 0:
                self.Cold = 1
                return ActionSkillCold
        if helper.get_remain_time() == 30:
            if self.Power == 0:
                self.Power = 1
                return ActionSkillPower
        if helper.get_remain_time() == 15:
            if self.Invisible == 0:
                self.Invisible = 1
                return ActionSkillInvisible
        
        r = helper.get_field_radius()
        r_me = helper.me.get_position_r()[0]
        if r_me >= r*7/10 and (r - r_me) >= 500*helper.me.get_speed()[0]/1000 and SKILL["Swap"] >= 1:
            #print("okok")
            count = 0
            for other_player in helper.get_others_degree():
                if helper.me.cal_vector(other_player.get_position())[0] <= 200:
                    count += 1
            
            if count >= 1 :
                swapplayer = None
                for other_player in helper.get_others_degree():
                    if swapplayer == None or other_player.get_position_r()[0] <= swapplayer.get_position_r()[0]:
                        swapplayer = other_player
                return ActionSkillSwap, other_player
                  
        if not helper.get_cards():
            if self.click:
                self.click = False
                return ActionUnPress

            if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
                self.target = None
                for other_player in helper.get_others_degree():
                    ok = True
                    deg = helper.cal_vector(other_player.get_position())[1]
                    for wall in helper.get_walls():
                        if wall[0] <= deg and deg <= wall[1]:
                            ok = False
                            break
                    if ok :
                        self.target = other_player
                if self.target == None:
                    self.back = True
                #self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻
                r = helper.get_field_radius()
                r_me = helper.me.get_position_r()[0]
                #print("BACK", self.back)
                if self.back == False:
                    while r_me > r * 7 / 12:
                        if helper.me.check_facing((0, 0)) == True:
                            self.back = True
                            return ActionPress
                        else:
                            return ActionNull
                else:
                    
                    if r_me < r * 7 / 12:
                        self.back = False
                        return ActionUnPress
                    else:
                        if helper.me.check_facing((0, 0),0.4):
                            return ActionPress
                        else:
                            return ActionUnPress
            else:
                ok = True
                deg = helper.cal_vector(self.target.get_position())[1]
                for wall in helper.get_walls():
                    if wall[0] <= deg and deg <= wall[1]:
                        ok = False
                        break
                if not ok :
                    self.target = None
                    return ActionUnPress
                if helper.me.check_facing(self.target.get_position(),0.145):
                    return ActionPress
                else:
                    if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                        self.click = True # 輕按一下等一下馬上放掉
                        return ActionPress
                    return ActionUnPress
        else :
            #print(2)
            cards = helper.get_cards()
            
            for card in cards:
                if self.click:
                    self.click = False
                    return ActionUnPress
                if helper.me.check_facing(card,0.145):
                    return ActionPress
                else:
                    pass
                    if helper.me.cal_rel_degree(card) > math.pi: # 如果要轉超過180度
                        self.click = True # 輕按一下等一下馬上放掉
                        return ActionPress
                    return ActionUnPress
            return ActionUnPress