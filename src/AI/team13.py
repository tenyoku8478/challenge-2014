'''
    team1 ai test
'''
import math
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "精準定位"
        self.ESP = 0.2
        self.click = False

    def decide(self, helper):
        others = helper.get_others_degree()
        if self.click:
            self.click = False
            return AI.ai_config.ActionUnPress

        if len(others) == 0:
            return AI.ai_config.ActionUnPress

        other = others[0]
        if helper.me.check_facing(other.get_position(), self.ESP):
            self.ESP = 0.4
            return AI.ai_config.ActionPress
        else:
            self.ESP = 0.1
            if helper.me.cal_rel_degree(others[0].get_position()) > 2*math.pi - helper.me.cal_rel_degree(others[-1].get_position()) + self.ESP:
                self.click = True
                return AI.ai_config.ActionPress
            return AI.ai_config.ActionUnPress
