'''
    AI template
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "煞氣a六小" # 隊伍名稱
        self.Co = 1
        self.Po = 2
        self.chase = True
        self.double = True
        self.ie = True
        self.inv = True
        self.target = None
        self.click = False
        self.swap = False
        self.time = 120
        self.ie_time = 120
        self.Sc = 3
        self.ie_action = 58
        self.double_action = False
		# 這裡可以放變數暫存一些資訊

    def decide(self, helper):
        # 在這裡設計屬於你們的 AI 吧！
        r, theta = helper.me.get_position_r()  #自己的極座標
        radius = helper.get_field_radius()
        time = helper.get_remain_time()
        if (self.ie_time-time)>=10:
            self.ie = True
        if helper.get_remain_time() == 16 and self.inv:
            self.inv = False
            return ActionSkillInvisible
        if helper.get_remain_time() == self.ie_action and self.ie:
            self.ie = False
            self.double_action = True
            self.ie_action -= 10
            return ActionSkillCold
        if self.double_action:
            self.double_action = False
            return ActionSkillScoreDouble
        if helper.get_remain_time()== 60 and self.double:
            self.double = False
            return ActionSkillScoreDouble
        if helper.get_remain_time()== 60 and self.chase:
            self.chase = False
            return ActionSkillChaseBeat
        
        
        if (self.time-time)>=3:
            self.swap = False
        if self.swap == False and helper.me.is_lock():
            if r > radius*8/10:
                
                others = helper.get_others_distance()
                 
                temp = radius
                temp_player = helper.me
                for x in others:
                    radius012, theta012 = x.get_position_r()
                    if temp > radius012:
                        temp = radius012
                        temp_player = x

                self.swap = True
                self.time = helper.get_remain_time()
                return ActionSkillSwap,temp_player
            
        
        if self.Po ==2 :    #發動霸氣
            self.Po =0
            return ActionSkillPower
        if self.Co ==1 :    #發動你有聽過lag嗎
            self.Co =0
            return ActionSkillCold
        if self.Sc == 3 :    #加倍奉還
            self.Sc =0
            return ActionSkillScoreDouble
        
        if self.click:
            self.click = False
            return ActionUnPress
		
        if self.target == None or self.target.is_dead():
            other = helper.get_others_distance()
            self.target = other[-1]
        if helper.me.check_facing(self.target.get_position(),0.05*math.pi):
            return ActionPress
        else:
            
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
                self.click = True # 輕按一下等一下馬上放掉
                return ActionPress
            
            #return ActionUnPress	
		
	    #if helper.me.check_facing(self.target.get_position()):
        #   return ActionPress
        
			
        rad = helper.get_field_radius()
        z = rad - 10
        pos = (1, 1) 
        r, theta = helper.cal_vector(pos) #換算成極座標
        if r <= rad and r > z:
            if player.check_facing( (0,0) ):
                return ActionPress
            else: 
                return ActionUnPress
        #else:
        #    return ActionNull
			
        return ActionUnPress
