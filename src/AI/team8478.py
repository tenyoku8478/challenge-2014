'''
    team1 ai test
'''
import math
from AI.base_ai import BaseAi
import AI.ai_config
class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "龜中間"
        self.EPS = 0.2
        self.cnt = 0

    def decide(self, helper):
        if helper.me.check_facing((0, 0), self.EPS):
            self.cnt = 10
            self.EPS = 0.4
            return AI.ai_config.ActionPress
        else:
            if self.cnt > 0:
                self.cnt -= 1
                return AI.ai_config.ActionPress
            else:
                self.ESP = 0.2
                return AI.ai_config.ActionUnPress
