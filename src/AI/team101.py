import math
from AI.base_ai import BaseAi
from AI.ai_config import *

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "吃卡片"

    def decide(self, helper):
        cards = helper.get_cards()
        for card in cards:
            if helper.me.check_facing(card):
                return ActionPress
        return ActionUnPress
