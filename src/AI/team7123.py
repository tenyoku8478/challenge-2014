import math
from AI.base_ai import BaseAi
import AI.ai_config

SKILL = {
    "Invisible" : 4, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 3, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "skill hh"
        self.EPS = 0.2
        self.click = False
        self.inv = [(120,4), (105,3), (90,2), (75,1)]
        self.abs = [(60,3), (50,2), (40,1)]

    def decide(self, helper):
        if (helper.get_remain_time(), SKILL["Invisible"]) in self.inv:
            return AI.ai_config.ActionSkillInvisible
        if (helper.get_remain_time(), SKILL["Abs"]) in self.abs:
            return AI.ai_config.ActionSkillAbs
        if self.click:
            self.EPS = 0.2
            self.click = False
            return AI.ai_config.ActionUnPress
        cards = helper.get_cards()
        if len(cards) > 0:
            if helper.me.check_facing(cards[0], self.EPS):
                self.EPS = 0.4
                return AI.ai_config.ActionPress
            else:
                if helper.me.cal_rel_degree(cards[0]) > 2*math.pi - helper.me.cal_rel_degree(cards[0]) + self.EPS:
                    self.click = True
                    return AI.ai_config.ActionPress
            self.EPS = 0.2
            return AI.ai_config.ActionUnPress    
        attack_list = helper.get_others_degree()
        if len(attack_list) == 0:
            if helper.me.check_facing((0,0), self.EPS):
                self.EPS = 0.4
                return AI.ai_config.ActionPress
            else:
                if helper.me.cal_rel_degree((0,0)) > 2*math.pi - helper.me.cal_rel_degree((0,0)) + self.EPS:
                    self.click = True
                    return AI.ai_config.ActionPress
                return AI.ai_config.ActionUnPress
            self.EPS = 0.2
            return AI.ai_config.ActionUnPress
        if helper.me.check_facing(attack_list[0].get_position(), self.EPS):
            self.EPS = 0.4
            return AI.ai_config.ActionPress
        else:
            if helper.me.cal_rel_degree(attack_list[0].get_position()) > 2*math.pi - helper.me.cal_rel_degree(attack_list[-1].get_position()) + self.EPS:
                self.click = True
                return AI.ai_config.ActionPress
        self.EPS = 0.2
        return AI.ai_config.ActionUnPress
