'''
    AI template
'''
'''
重複run1~6
1. ok
if  其他三者皆存在  且他們之r值>=R/2 and自己之r值<=R/2
發動 霸王色霸氣 (順位1)
2. error
if 敵人變小時 優先鎖定攻擊 (順位2)
一般情況 鎖定分數最高者 攻擊
偵測不到他時  
則 找"r較大之敵人" 攻擊 
    if 己之r值>=目標時  止步 找下個目標
3. ok
while 驚嘆號在場上時  (順位3)
  if 往驚嘆號的路徑上無敵人
    則 前往取得道具
  else 向旁邊轉個角度前進一小段距離
4. ok
if  其他三者分數皆高過自己 且己方不在使用技能中的狀態 
or if 比賽時間過半 且自己之積分不是最高者 且己方不在使用技能中的狀態 (順位4)
  發動  你聽過lag嗎
  if 已無"你聽過lag嗎" 則發動 一大堆香蕉
5. 
if 敵人變大或變快且積分高於己方時(順位5)
  and if 三秒後無他人使用技能卡 發動 進擊的巨人
  if 無技能可用時
  走回中心 
  定位所有未開技能之人
  找出r值大的 攻擊 
  if 己之r值>=變大敵人and目標時 返回中心
6.
if 己方分數最高且比賽時間剩下40秒之內 且己方不在使用技能中的狀態 (順位6)
  三秒後 發動 你看不到我
  優先攻擊 發動加倍奉還者
  若無 則往中心點移動 定位 
  找r值最大的敵人攻擊  
  且if r值>=敵人時 返回中心
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 3, # 來 你來 來
    "Power": 3, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}
SKILL["Cold"] == 0
class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "SORRY囉~" # 隊伍名稱
        self.target = None
        self.click = False
        self.skill_count = 120
        self.power=2
        self.count=2
        self.tmp=-5
        self.double=1
        # 這裡可以放變數暫存一些資訊

    def decide(self, helper):
        #skill
        # 霸王色的霸氣
        if helper.others and helper.me.get_skill_count('Power') != 0 and helper.get_remain_time() % 60 == 20:
            for player in helper.get_others_distance():
                if (not player.is_dead()) and (player.get_position_r()[0] >= helper.get_field_radius()*1/2) and (helper.me.get_position_r()[0] <= helper.get_field_radius()*1/2) :
                    self.skill_count=helper.get_remain_time()
                    return ActionSkillPower
        
        elif helper.get_remain_time() % 60 == 30 :
            return ActionSkillInvisible
        elif helper.get_remain_time() %5 == 0 and helper.get_remain_time()-self.tmp>0 and self.count > 0 and helper.me.get_position_r()[0] > helper.get_field_radius() * 0.75 :
            self.count-=1
            rrrrr= helper.get_field_radius()
            self.tmp=helper.get_remain_time()
            for player in helper.get_others_distance() :
                if player.get_position_r()[0]<rrrrr :
                    rrrrr=player.get_position_r()[0]
                    pp=player
            return ActionSkillSwap, pp
        
        elif (helper.get_remain_time() % 60 == 15 or helper.get_remain_time() % 60 == 45) and  len(helper.get_others_score())>0:
            if helper.me.get_score() < helper.get_others_score()[len(helper.get_others_score())-1].get_score():
                if helper.me.get_skill_count('Cold')!=0 :
                    return ActionSkillCold
                else:
                    return ActionSkillChaseBeat
        elif helper.get_remain_time() % 60 == 40:
            if helper.me.get_skill_count('Cold')!=0:
                return ActionSkillAbs
            else:
                return ActionSkillChaseBeat
        elif helper.get_remain_time() == 60 and self.double!=0 :
            self.double=0
            return ActionSkillScoreDouble
        # 敵人變小時攻擊 優先鎖定攻擊  一般情況下 鎖定分數最高者攻擊
        '''
        small = []
        for player in helper.get_others_score():
            if player.get_mass()==2 :
                self.target = player
                break
            else:
                self.target = player
                break
                return ActionPress  
            if helper.me.get_position_r()[0]>=player.get_position_r()[0]:
                return ActionUnPress  #if 己之r值>=目標時(這表示沒撞到人)   止步 找下個目標
        '''
        # when big
        if helper.me.get_mass()==6 and helper.get_others_distance()[0].check_facing(helper.me.get_position(),0.1):
            return ActionPress
        # time half
        #elif time < 1/2
        # walk
        elif self.click:
            self.click = False
            return ActionUnPress
        elif self.target == None :
            self.target = helper.get_others_degree()[0]   # 最快會轉到的那隻
        elif self.target.is_dead():
            self.target = helper.get_others_degree()[1] 
            return ActionUnPress
        elif helper.me.check_facing(self.target.get_position(),0.1):
            return ActionPress
        elif helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
            self.click = True # 輕按一下等一下馬上放掉
            return ActionPress
        cards = helper.get_cards()
        for card in cards:
            if helper.me.check_facing(card):
                return ActionPress
        else :
            return ActionUnPress
