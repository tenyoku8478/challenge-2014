'''
    AI template
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}

class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "team7" # 隊伍名稱
        #self.dis=get_field_radius()
        self.target=None
        self.click = False
        self.swaptime = 120
        self.invi_time = 999
        #
        self.useswap=False
        self.usedouble=False
        self.useinvisi=False
        self.usepower=False
        self.usechase=False
        self.useabs=False
        #
        self.actIE=False
        # 這裡可以放變數暫存一些資訊

    def decide(self, helper):
        useIE = True
        for player in helper.get_others_score():
            if player.get_position_r()[0] < helper.get_field_radius()/2:
                useIE = False
                break
        if SKILL["ScoreDouble"] > 0 and self.actIE == True:
            return ActionSkillScoreDouble

        if SKILL["ChaseBeat"] > 0 and helper.me.get_score() < helper.get_others_score()[0].get_score() and useIE:
            self.actIE=True
            return ActionSkillChaseBeat
        #print(helper.me.get_size
        if self.click:
            self.click = False
            return ActionUnPress
        #print(helper.get_field_radius())   225.0
        # 在這裡設計屬於你們的 AI 吧！
        eps=0.9
        dis=helper.get_field_radius()/2
        dis_abs=100
        dis_time=60
        delta=20
        if helper.me.get_size()>35:
            delta=0
            eps=0.3
        #### skill
        if (not self.useabs)and(helper.me.get_position_r()[0]<dis_abs)and(helper.get_remain_time()<dis_time) and helper.me.get_score() < helper.get_others_score()[0].get_score():
            self.useabs=helper.get_remain_time()
            return ActionSkillAbs
        if (self.useabs!=None) and (helper.get_remain_time()<=30) and (helper.get_remain_time()>=10) and (helper.get_remain_time()<self.useabs-10):
            if (helper.me.get_position_r()[0]<dis_abs):
                me_s=helper.me.get_score()
                ll=len(me_s)
                ta=helper.get_others_score()
                if not(ll==0 or me_s<ta[ll].get_score()-500 or me_s>ta[0].get_score()+300):
                    self.actIE=True
                    self.useabs=True   ######1111111111111111111111111111111111111111111
                    return ActionSkillScoreDouble
        if (self.usepower==False):
            rr=helper.me.get_position_r()[0]
            if rr>helper.get_field_radius()/3 and rr<helper.get_field_radius()*2/3:
                ta=helper.get_others_score()
                count=0
                for x in ta:
                    if x.cal_vector(helper.me.get_position())[0]-helper.me.get_size()-x.get_size()<30:
                        count=count+1
                if count>=2:
                    self.usepower=True
                    return ActionSkillPower
        ####
        if self.target != None and (self.target.is_dead() or self.target.is_invisible):########1
            self.target=None
        #print('a')
        if self.target!=None and self.target.get_position_r()[0]>dis:
            tmp=self.target.get_position()
            to_x=None
            to_y=None
            if tmp[0]>0:
                to_x=tmp[0]-delta
            else:
                to_x=tmp[0]+delta
            if tmp[1]>0:
                to_y=tmp[1]-delta
            else:
                to_y=tmp[1]+delta
        if SKILL["Swap"] <= 0 and self.swaptime-3 > helper.get_remain_time() and (self.useinvisi<2)and(helper.me.get_position_r()[0]>150) and self.invi_time-15 > helper.get_remain_time():
            ta=helper.get_others_score()
            self.invi_time = helper.get_remain_time()
            for x in ta:
                if x.check_facing(helper.me.get_position(),eps) and x.get_speed()[0]>0:
                    self.useinvisi=self.useinvisi+1
                    return ActionSkillInvisible
        if helper.me.get_position_r()[0] > 160 and self.swaptime - helper.get_remain_time() > 3:
            self.swaptime = helper.get_remain_time()
            d = 225
            i = 0
            c = 0
            while i < len(helper.others):
                if helper.others[i].get_position_r()[0] < d:
                    d = helper.others[i].get_position_r()[0]
                    c = i
                i += 1
            return ActionSkillSwap, helper.others[c]
        #print('b')
        kk=helper.get_others_score()
        #self.target=None
        if self.target == None:
            for i in kk:
                if i.get_position_r()[0]>dis and i.get_mass() <= helper.me.get_mass() and i.get_position_r()[0] > helper.me.get_position_r()[0]:
                    self.target=i
                    break
        if self.target==None:
            if helper.me.check_facing((0,0),eps):
                return ActionPress
            return ActionUnPress
        #print('c')
        tmp=self.target.get_position()
        to_x=None
        to_y=None
        if tmp[0]>0:
            to_x=tmp[0]-delta
        else:
            to_x=tmp[0]+delta
        if tmp[1]>0:
            to_y=tmp[1]-delta
        else:
            to_y=tmp[1]+delta
        to=(to_x,to_y)
        if helper.me.check_facing(to,eps):
            return ActionPress
        if helper.me.cal_rel_degree(to) > math.pi: # 如果要轉超過180度
            self.click = True # 輕按一下等一下馬上放掉
            return ActionPress
        #print('d')
        return ActionUnPress
