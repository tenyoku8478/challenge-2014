'''
    need fix: 遊戲一開始就開進擊的巨人
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

# 特殊技能數量
SKILL = {
    "Invisible" : 0, # 你看不到我
    "Swap": 0, # 來 你來 來
    "Power": 0, # 霸王色霸氣
    "Abs": 0, # 進擊的巨人
    "ChaseBeat": 0, # 一大堆香蕉
    "Cold": 0, # 你聽過 Lag 嗎
    "ScoreDouble": 0 # 加倍奉還
}
class TeamAI(BaseAi):
    def __init__(self, env):
        BaseAi.__init__(self)
        self.name = "FIVE"
        self.target = None
        self.click = False
        self.Swapused = 0
        self.Coldused = 0
        self.bachi = 0
        
    def decide(self, helper):
        #進擊的巨人
        if helper.me.get_skill_count('Abs') > 0:
            return ActionSkillAbs

        #短時間內不要連續用swap
        if self.Swapused is not 0:
            self.Swapused -= 1
        if self.Coldused is not 0:
            self.Coldused -= 1
        if self.bachi is not 0:
            self.bachi -= 1

        #設定目標
        if self.target and self.target.is_dead():
            self.target = None
        if self.target == None:
            if helper.get_others_degree():
                self.target = helper.get_others_degree()[0]
 
        #不要自殺
        if helper.me.cal_vector((0,0))[0] == helper.get_field_radius() :
             return ActionUnPress
        
        #輕按
        if self.click:
            self.click = False
            return ActionUnPress
        
        #接近邊界時和最遠目標交換            
        if helper.others and helper.me.cal_vector((0,0))[0] +30 > helper.get_field_radius():
            if helper.me.get_skill_count('Swap') > 0:
                Swaptarget = helper.get_others_distance()[-1] 
                if self.Swapused == 0:
                    self.Swapused = 20
                    return ActionSkillSwap, Swaptarget
            elif helper.me.get_skill_count('Cold') > 0:
                if self.Coldused == 0:
                    self.Coldused = 150
                    return ActionSkillCold
  
        #吃卡片
        cards= helper.get_cards()
        for card in cards:
            if helper.me.check_facing(card):
                return ActionPress
            else:
                if helper.me.cal_rel_degree(card) > math.pi: # 如果要轉超過180度
                    self.click = True # 輕按一下等一下馬上放掉
                    return ActionPress

        #中間開霸氣&香蕉
        if helper.me.get_position_r()[0] < 60:
            if self.bachi == 0:
                self.bachi = 150
                if helper.me.get_skill_count('Power') > 0:
                    return ActionSkillPower
                elif helper.me.get_skill_count('ChaseBeat') > 0:
                    return ActionSkillChaseBeat
            

        #變小龜中間        
        if helper.me.get_size() < 60 and helper.me.get_size() > 40 :
            if helper.get_position()[0] > helper.get_field_radius() *1/2 and helper.me.check_facing((0,0)):
                return ActionPress
         
        #別人變大時隱形
        for other in helper.others:
            if other.get_size() > helper.me.get_size():
                if not helper.me.is_invisible() and helper.me.get_skill_count('Invisible') > 0:
                    return ActionSkillInvisible
        
        #判斷大小
        if self.target and helper.me.check_facing(self.target.get_position()):
            if helper.get_others_degree()[0].get_size() > helper.me.get_size():
                 return ActionUnPress

        #瞄準 bug: 會一直抖
        if self.target:
            if helper.me.cal_vector(self.target.get_position())[0] > 100 :
                if helper.me.check_facing(self.target.get_position(), 0.2) :
                    return ActionPress
            else:
                if helper.me.check_facing(self.target.get_position(), 0.5) :
                    return ActionPress
            #反轉
            if helper.me.cal_rel_degree(self.target.get_position()) > math.pi:
                self.click = True
                return ActionPress

        return ActionUnPress
'''
在別人變大時隱形
在中心區域時開霸氣+香蕉
在邊邊時開啟LAG
'''
