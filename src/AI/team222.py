'''
    skill swap AI demo
'''
import math
from AI.base_ai import BaseAi
from AI.ai_config import *

SKILL = {"Swap" : 1}

class TeamAI(BaseAi):
    def __init__(self,env):
        BaseAi.__init__(self)
        self.name = "Swap"

    def decide(self, helper):
        # 如果場上沒有其他人或沒招式卡片了就不按，否則就按
        if not helper.others or helper.me.get_skill_count('Swap') == 0:
            return ActionUnPress
        else: 
            # 放招時同時回傳要與誰交換
            return ActionSkillSwap, helper.others[0]
