import math
from AI.base_ai import BaseAi
from AI.ai_config import *

SKILL = {
    "Invisible" : 3, # 你看不到我
    "Swap": 2, # 來 你來 來
    "Power": 1, # 霸王色霸氣
    "Abs": 1, # 進擊的巨人
    "ChaseBeat": 1, # 一大堆香蕉
    "Cold": 100, # 你聽過 Lag 嗎
    "ScoreDouble": 3 # 加倍奉還
}

class TeamAI(BaseAi):
	def __init__(self,env):
		BaseAi.__init__(self)
		self.name = "羊貓龍"
		self.target = None
		self.click = False
		self.where = False
		self.player = None
		self.k1 = 0
		self.k2 = 0
		self.p = 0
		self.t = 200

	def decide(self, helper):
		#print(helper.get_remain_time())
		a = helper.get_remain_time()
		player = helper.me
		r = helper.get_field_radius()

		if player.get_size() == 35:
			self.p = 0
			self.k1 = 0
			self.k2 = 0 
		
		if helper.get_remain_time() < 10:
			if SKILL['Power'] > 0:
				return ActionSkillPower
			if SKILL['Abs'] > 0:
				return ActionSkillAbs
			if SKILL['ChaseBeat'] > 0:
				return ActionSkillChaseBeat
			if SKILL['ScoreDouble'] > 0:
				return ActionSkillScoreDouble
		
		if player.get_size() == 62.5 and self.k1 == 0: #「加倍奉還」
			self.k1 = 1
			if SKILL["ScoreDouble"]>0:
				return ActionSkillScoreDouble
			
		if player.get_size() == 62.5 and self.k2 ==0: #「你看不到我」
			self.k2 = 1
			if SKILL["Invisible"]>0:
				return ActionSkillInvisible
			
		if player.cal_vector( (0,0) )[0] >= r*0.514 and (self.t - a)>10: #「來來來你來」
			self.t = a
			if SKILL["Swap"]>0:
				if player.is_lock():			
					return ActionSkillSwap, helper.get_others_distance()[1] 
		
		if player.get_size() == 25 and self.p==0: #「霸王色霸氣」
			self.p = 1
			if SKILL["Power"]>0:
				return ActionSkillPower			
		
		if player.get_size() == 25 and self.p==0 : #「進擊的巨人」
			self.p = 1
			if SKILL["Abs"]>0:
				return ActionSkilllAbs
			
		if player.cal_vector( (0,0) )[0] < r/5: #「一大堆香蕉」
			if SKILL["ChaseBeat"]>0:
				return ActionSkillChaseBeat
		
		cards = helper.get_cards()
		for card in cards:
			if player.check_facing(card):
				return ActionPress
			return ActionUnPress

		if self.click:
			self.click = False
			return ActionUnPress

		if self.target == None or self.target.is_dead():
			self.target = helper.get_others_degree()[0]
			
		if helper.me.check_facing(self.target.get_position(),0.514):
				return ActionPress
		else:
			if player.cal_rel_degree(self.target.get_position()) > math.pi:
				self.click = True
				return ActionPress
				
			return ActionUnPress	
					
		'''
		cards = helper.get_cards()
		if len(cards) ==0 :
			if self.target == None or self.target.is_dead(): # 如果沒有目標或目標已死
				self.target = helper.get_others_degree()[0] # 鎖定最快會轉到的那隻

			if helper.me.check_facing(self.target.get_position(),0.1256):
				return ActionPress	
			else:
				if helper.me.cal_rel_degree(self.target.get_position()) > math.pi: # 如果要轉超過180度
					self.click = True # 輕按一下等一下馬上放掉
					return ActionPress
					
			return ActionUnPress
		else:
			for card in cards:
				if helper.me.check_facing(card, 0.1256):
					return ActionPress
		'''		

