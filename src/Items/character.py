import math

import Config.env_config as ENV_CON

from Items.item_base import BaseItem

class Character(BaseItem):
    def __init__(self, dic):
        BaseItem.__init__(self, dic)
        self.reset(0)

        self.magic_timestamp = 0 # second

        self.mass_tick = 0
        self.radius_tick = 0
        self.acc_tick = 0

        self.magic_smile_timestamp = 0

        self.super_lock_timestamp = 0
        
        self.acc = 300 

    def magic(self):
        self.magic_smile_timestamp = 2

    def super_lock(self, time):
        self.super_lock_timestamp = time

    def is_magic_smile(self):
        return (self.magic_smile_timestamp > 0)

    def magic_tick(self, timestamp):

        def ticking(timestamp, delta):
            if(abs(timestamp) <= delta) : return 0
            elif timestamp > 0 : return timestamp - delta
            elif timestamp < 0 : return timestamp + delta

        delta = timestamp - self.magic_timestamp
        self.magic_timestamp = timestamp

        self.magic_smile_timestamp = max(0, self.magic_smile_timestamp - delta)
        self.super_lock_timestamp = max(0, self.super_lock_timestamp - delta)

        self.mass_tick = ticking(self.mass_tick, delta)
        self.radius_tick = ticking(self.radius_tick, delta)
        self.acc_tick = ticking(self.acc_tick, delta)

        if self.mass_tick < 0:
            self.mass = ENV_CON.PLAYER_MASSES[0]
        elif self.mass_tick == 0:
            self.mass = ENV_CON.PLAYER_MASSES[1]
        elif self.mass_tick > 0:
            self.mass = ENV_CON.PLAYER_MASSES[2]

        if self.radius_tick < 0:
            self.radius = ENV_CON.PLAYER_SIZES[0]/2
        elif self.radius_tick == 0:
            self.radius = ENV_CON.PLAYER_SIZES[1]/2
        elif self.radius_tick > 0:
            self.radius = ENV_CON.PLAYER_SIZES[2]/2

        if self.acc_tick < 0:
            self.acc = ENV_CON.PLAYER_ACCS[0]
        elif self.acc_tick == 0:
            self.acc = ENV_CON.PLAYER_ACCS[1]
        elif self.acc_tick > 0:
            self.acc = ENV_CON.PLAYER_ACCS[2]

        if self.lock <= 0:
            self.lock = self.super_lock_timestamp

    def get_mass_level(self):
        if self.mass_tick > 0 : return 1
        elif self.mass_tick < 0 : return -1
        else : return 0

    def super_lock(self, time):
        self.super_lock_timestamp = time

    def magic_acc(self, time):
        self.acc_tick += time

    def magic_mass(self, time):
        self.mass_tick += time

    def magic_radius(self, time):
        self.radius_tick += time

    def get_larger_radius(self):
        if self.radius == ENV_CON.PLAYER_SIZES[0]/2: return ENV_CON.PLAYER_SIZES[1]/2
        if self.radius == ENV_CON.PLAYER_SIZES[1]/2: return ENV_CON.PLAYER_SIZES[2]/2
        if self.radius == ENV_CON.PLAYER_SIZES[2]/2: return ENV_CON.PLAYER_SIZES[2]/2

    def reset(self, timestamp):
        self.rotate_dir = 1 # clock or counterclock
        self.pressed = False # if pressed the key
        self.acceleration = 0
        self.invisible_time = 0
        self.lock = 0
        self.update_timestamp = timestamp
        self.speed = 0

    def press(self):
        self.pressed = True

    def unpress(self):
        self.pressed = False

    def get_center(self):
        return self.position['x'] , self.position['y']
