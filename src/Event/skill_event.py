import pygame

from Event.base_event import BaseEvent

import Event.game_event
import Event.physics_event

import Config.event_config as EVENT_CON
import Config.game_config as GAME_CON
import Config.env_config as ENV_CON
import Config.skill_config as SKI_CON
import AI.ai_config as AI_CON

from Items.ball import Ball

from Tools.vector import Vector

class EventSkillInvisible(BaseEvent):
    def __init__(self, env, player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/invisible.wav')
        self.env = env
        self.player = player
        self.priority = ms

    def do_action(self):
        self.sound.play()
        self.player.magic_invisible(SKI_CON.INVISIBLE_TIME)


class EventSkillSwap(BaseEvent):
    def __init__(self, env, trigger_player, toward_player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/swap.wav')
        self.env = env
        self.trigger_player = trigger_player
        self.toward_player = toward_player
        self.priority = ms

    def do_action(self):
        self.sound.play()
        if self.toward_player in self.env['live_player'] and self.trigger_player in self.env['live_player'] :
            gx, gy = self.trigger_player.char.get_center()
            wx, wy = self.toward_player.char.get_center()
            self.trigger_player.char.position = dict(x = wx, y = wy)
            self.toward_player.char.position = dict(x = gx, y = gy)


class EventSkillAddWall(BaseEvent):
    def __init__(self, env, wall_index_list, ms):
        self.env = env
        self.wall_index_list = wall_index_list
        self.priority = ms

    def do_action(self):
        for wall_index in self.wall_index_list:
            ww = self.env["const_walls"][wall_index]
            self.env["gamec"].add_event(Event.game_event.EventAddWall(self.env, ww, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventSkillScoreDouble(BaseEvent):
    def __init__(self, env, player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/double.wav')
        self.env = env
        self.player = player
        self.priority = ms

    def do_action(self):
        self.sound.play()
        self.player.magic_score(SKI_CON.SCOREDOUBLE_TIME)


class EventSkillPower(BaseEvent):
    def __init__(self, env, player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/power.wav')
        self.env = env
        self.player = player
        self.priority = ms

    def do_action(self):
        self.sound.play()
        items = []
        for ball in self.env["balls"]:
            items.append(ball)

        for player in self.env["player"]:
            items.append(player.char)

        self.env["gamec"].add_event(Event.physics_event.EventBachi(self.player.char, items, self.priority + EVENT_CON.TICKS_PER_TURN))


class EventSkillChaseBeat(BaseEvent):
    def __init__(self, env, trigger_player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/chase_beat.wav')
        self.env = env
        self.trigger_player = trigger_player
        self.priority = ms

    def do_action(self):
        self.sound.play()
        for _times, other_player in enumerate(self.env["player"]):
            if other_player is self.trigger_player: continue
            self.env["gamec"].add_event(EventSkillChaseBall(self.env, self.trigger_player, other_player, 2, self.priority + EVENT_CON.TICKS_PER_TURN + _times*1000))


class EventSkillChaseBall(BaseEvent):
    def __init__(self, env, trigger_player, target_player, times, ms):
        self.env = env
        self.target_player =  target_player
        self.trigger_player = trigger_player
        self.priority = ms
        self.times = times

    def do_action(self):
        if self.times == 0: return
        if(self.target_player not in self.env["live_player"]) or self.target_player.invisible:
            self.env["gamec"].add_event(EventSkillChaseBall(self.env, self.trigger_player, self.target_player, self.times, self.priority + EVENT_CON.TICKS_PER_TURN + 500))
            return
        import math
        import random
        ok = True

        x, y = self.target_player.char.get_center()
        dx = ENV_CON.ROOM_CTR[0] - self.target_player.char.position['x']
        dy = ENV_CON.ROOM_CTR[1] - self.target_player.char.position['y']
        vv = Vector(dx, dy)
        vv = vv*100/vv.len()
        if vv.len() <=  0.1 :
            ok = False

        px, py = x + vv.x, y + vv.y

        for ball in self.env["balls"]:
            x1, y1 = ball.get_center()
            if (px - x1)**2 + (py - y1)**2 < ENV_CON.BALL_SIZE :
                ok = False
                break

        for other_player in self.env["live_player"]:
            if other_player == self.target_player : continue
            x1, y1 = other_player.char.get_center()
            if (px - x1)**2 + (py - y1)**2 < ENV_CON.BALL_SIZE//2 + other_player.char.radius :
                ok = False
                break

        for wall in self.env["walls"]:
            deg = vv.get_degree() + math.pi
            if deg >= 2*math.pi:
                deg -= 2*math.pi
            if(deg <= wall.theta2 and deg >= wall.theta1):
                ok = False
                break

        if ok == False:
            self.env["gamec"].add_event(EventSkillChaseBall(self.env, self.trigger_player, self.target_player, self.times, self.priority + EVENT_CON.TICKS_PER_TURN + 500))
            return
        delta_degree = (random.randint(0, 801) - 400) / 10000
        self.target_player.char.super_lock(5)
        bb = Ball(dict(
            speed = ENV_CON.BALL_SPEED,
            position = dict(x = x + vv.x, y = y + vv.y),
            radius = ENV_CON.BALL_SIZE / 2,
            degree = math.pi + vv.get_degree() + delta_degree,
            mass = ENV_CON.BALL_MASS,
            timestamp = self.priority,
        ))
        bb.trigger_player = self.trigger_player
        self.env["gamec"].add_event(Event.game_event.EventAddBall(self.env, bb, self.priority + EVENT_CON.TICKS_PER_TURN))
        self.env["gamec"].add_event(EventSkillChaseBall(self.env, self.trigger_player, self.target_player, self.times-1, self.priority + EVENT_CON.TICKS_PER_TURN + 1000))


class EventSkillAbs(BaseEvent):
    def __init__(self, env, trigger_player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/abs.wav')
        self.priority = ms
        self.env = env
        self.trigger_player = trigger_player

    def do_action(self):
        self.sound.play()
        self.trigger_player.char.magic_mass(SKI_CON.ABS_TIME)
        self.trigger_player.char.magic_radius(SKI_CON.ABS_TIME)
        for other_player in self.env["player"]:
            if other_player is self.trigger_player: continue
            other_player.char.magic_mass(-SKI_CON.ABS_TIME)
            other_player.char.magic_radius(-SKI_CON.ABS_TIME)


class EventSkillCold(BaseEvent):
    def __init__(self, env, trigger_player, ms):
        self.sound = pygame.mixer.Sound('sounds/skill/cold.wav')
        self.priority = ms
        self.env = env
        self.trigger_player = trigger_player

    def do_action(self):
        self.sound.play()
        for other_player in self.env["player"]:
            if other_player is self.trigger_player: continue
            other_player.magic_browser(SKI_CON.COLD_TIME)
