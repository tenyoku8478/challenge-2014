from sys import exit

import pygame ,math
import Event 
from Event.base_event import BaseEvent

from pygame.locals import *
import Config.event_config as EVENT_CON
import Config.env_config as ENV_CON
import Tools.Img as Img
import Event.skill_animation_event as Skill

ground_x = ENV_CON.ROOM_LEFT
ground_y = ENV_CON.ROOM_TOP
ground_xy = ground_x,ground_y

ground_wh = ENV_CON.ROOM_WIDTH, ENV_CON.ROOM_HEIGHT
bound = pygame.transform.scale(Img.Bound,ground_wh)

p_group = pygame.sprite.Group()
eye_group = pygame.sprite.Group()
wall_group = pygame.sprite.Group()
#card_group = pygame.sprite.Group()
ball_group = pygame.sprite.Group()
bound_group = pygame.sprite.Group()
died_group = pygame.sprite.Group()
muti_card = []

wall_xy = []
wall_xy.append((ground_x+225, ground_y))
wall_xy.append((ground_x+338, ground_y+31))
wall_xy.append((ground_x+420, ground_y+116))
wall_xy.append((ground_x+415, ground_y+230))
wall_xy.append((ground_x+331, ground_y+344))
wall_xy.append((ground_x+225, ground_y+420))
wall_xy.append((ground_x+115, ground_y+420))
wall_xy.append((ground_x+32, ground_y+341))
wall_xy.append((ground_x, ground_y+228))
wall_xy.append((ground_x, ground_y+116))
wall_xy.append((ground_x+31, ground_y+29))
wall_xy.append((ground_x+115, ground_y-2))
#ball
ball = pygame.image.load("screen/icon/IE.png")
w,h=ball.get_size()
s=0.25
w=int(w*s)
h=int(h*s)
ball = pygame.transform.smoothscale(ball,(w,h))
ball.fill((255, 255, 255, 255 * 0.75), None, pygame.BLEND_RGBA_MULT) 
class Model():
    def __init__(self):
        self.alpha = 0
        self.browser = ""
        self.position = (0,0)
        self.speed = 2
        self.degree = 0
        self.x = 0
        self.y = 0
        self.live = False
        self.eat = False
        self.num = 0
    def initset(self,browser,x,y,degree,direction,alpha):
        self.browser = browser
        self.x = x
        self.y = y
        self.degree = degree
        self.direction = direction
        self.alpha = alpha
    def move(self):
        d = math.degrees(self.direction)
        self.x += self.speed * math.cos(math.radians(d))
        self.y += self.speed * math.sin(math.radians(d))
        self.alpha -= 20
        if self.alpha < 0:
            self.alpha = 0

class Bound(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

bound_sprite = Bound(ground_xy, Img.Bound)
bound_group.add(bound_sprite)

class ball_sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = (x, y)

class Hole(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

class Card(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

class Wall(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

class Player(pygame.sprite.Sprite):
    def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position
        
class Eye(pygame.sprite.Sprite):
      def __init__(self,position,m):
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect = self.image.get_rect()
        self.rect.topleft = position

class EventInitGround(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
        self.surf = env["screen"]
        self.surf.blit(Img.Background, (0,0))
        env["model1"] = Model()
        env["model2"] = Model()
        env["model3"] = Model()
        env["model0"] = Model()
    def do_action(self):
        self.env["uic"].add_event(EventDrawGround(self.env, self.priority + EVENT_CON.TICKS_PER_TURN, 0))

class EventDrawGround(BaseEvent):
    def __init__(self, env, ms, animation):
        self.env = env
        self.priority = ms
        self.surf = env["screen"]
        self.animation = animation
        self.p = []
        self.walls = []
        self.cards = []
        self.died = []
        self.player_num = len(env["live_player"])
        self.wall_num = len(env["walls"])
        self.card_num = len(env["cards"])
        self.died_num = len(env["died_player"])
        for pp in self.env["live_player"]:
            self.p.append(pp)
        for pp in self.env["walls"]:
            self.walls.append(pp)
        for pp in self.env["cards"]:
            self.cards.append(pp) 
        for pp in self.env["died_player"]:
            self.died.append(pp) 
        self.balls = env["balls"]
        self.ie_num = 0 
    def set_ball(self):
        for tball in self.balls :
            tmp_sprite = ball_sprite(tball.position['x'] - 25,tball.position['y'] - 25,ball)
            ball_group.add(tmp_sprite)

    def set_wall(self):
        #do judge 
        for i in range(self.wall_num):
            w = Wall(wall_xy[self.walls[i].index], Img.Wall[self.walls[i].index])
            wall_group.add(w)
        #end
    def set_card(self):
        for i in range(self.card_num):
            card_group = pygame.sprite.Group()
            pos = self.cards[i].position['x'] - 22, self.cards[i].position['y'] - 25
            if self.cards[i].cardtype == 1 :
                image = Img.Chance[(self.animation % 24) // 4] 
            else:
                image = Img.Destiny[(self.animation % 24) // 4]
            temp_card = Card(pos, image)
            card_group.add(temp_card) 
            muti_card.append(card_group)

    def reset_group(self):
        p_group.empty()
        eye_group.empty()
        wall_group.empty()
        #card_group.empty()
        ball_group.empty()
        died_group.empty()
        #muti_card.empty()
        muti_card[:] = []
    def set_died(self,p):
        #set model
        index = "model" + str(p.index)
        if self.env[index].live:
            self.env[index].live = False
        self.env[index].move()
        alpha = self.env[index].alpha
        #judge degree
        degree = 270 - (math.degrees(p.char.degree))
        
        #judge browser and radius
        player_image = self.judge_browser_radius(p.browser, p.char.radius, 360-degree+7.5).copy()

        #judge eye
        if p.char.radius < 30:
            eye_image = Img.s_eye3[self.animation % 6].copy()
        elif p.char.radius < 50:
            eye_image = Img.n_eye3[self.animation % 6].copy()
        else:
            eye_image = Img.b_eye3[self.animation % 6].copy()
        #not finish 

        #set position
        player_w,player_h = player_image.get_size()
        player_pos = (self.env[index].x-player_w/2, self.env[index].y-player_h/2)


        #rotate eye and set position
        eye_r = ((p.char.radius*3)/2)-(eye_image.get_height()/2)
        eye_image = pygame.transform.rotozoom(eye_image, degree,1)
        eye_w,eye_h = eye_image.get_size()
        eye_pos = (self.env[index].x-eye_w/2+eye_r*math.cos(math.radians(degree+90))
            ,self.env[index].y-eye_h/2-eye_r*math.sin(math.radians(degree+90)))
        
        #set alpha
        player_image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        eye_image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        
        #add into sprite and group
        player = Player(player_pos, player_image)
        player_eye = Eye(eye_pos, eye_image)
        died_group.add(player)
        eye_group.add(player_eye)
    def set_player(self,p):
        #judge visible
        if not p.invisible:
            alpha = 255*0.8
        else:
            alpha = 255*0.25
        #set model
        index = "model" + str(p.index)
        if not self.env[index]:
            self.env[index].live = True
        self.env[index].alpha = 255
        self.env[index].initset(p.browser, p.char.position['x'], p.char.position['y'], p.char.degree, p.char.direction, alpha)
        
        
        #alpha = 255*0.8
        #judge degree
        degree = 270 - (math.degrees(p.char.degree))
        
        #judge browser and radius
        player_image = self.judge_browser_radius(p.browser, p.char.radius, 360-degree+7.5).copy()

        #judge eye
        eye_image = self.judge_eye(p.char.radius, p.char.lock ,p.char.is_magic_smile()).copy()
        #not finish 

        #set position
        player_w,player_h = player_image.get_size()
        player_pos = (p.char.position['x']-player_w/2, p.char.position['y']-player_h/2)


        #rotate eye and set position
        eye_r = ((p.char.radius*3)/2)-(eye_image.get_height()/2)
        eye_image = pygame.transform.rotozoom(eye_image, degree,1)
        eye_w,eye_h = eye_image.get_size()
        eye_pos = (p.char.position['x']-eye_w/2+eye_r*math.cos(math.radians(degree+90))
            ,p.char.position['y']-eye_h/2-eye_r*math.sin(math.radians(degree+90)))
        
        #set alpha
        player_image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        eye_image.fill((255, 255, 255, alpha), None, pygame.BLEND_RGBA_MULT)
        
        #add into sprite and group
        player = Player(player_pos, player_image)
        player_eye = Eye(eye_pos, eye_image)
        p_group.add(player)
        eye_group.add(player_eye)

    def judge_eye(self,radius,lock,smile):
        if radius < 30:
            if smile :
                image = Img.s_eye4[self.animation % 6]
            else:
                if lock > 0:
                    image = Img.s_eye2
                else:
                    image = Img.s_eye
        elif radius < 50:
            if smile :
                image = Img.n_eye4[self.animation % 6]
            else:
                if lock > 0:
                    image = Img.n_eye2
                else:
                    image = Img.n_eye
        else :
            if smile :
                image = Img.b_eye4[self.animation % 6]
            else:
                if lock > 0:
                    image = Img.b_eye2
                else:
                    image = Img.b_eye
        return image

    def judge_browser_radius(self,browser,radius,degree):
        while degree >= 360:
            degree -= 360
        while degree < 0:
            degree += 360
        if browser == "opera":
            if radius < 30:
                image = Img.s_opera[int(degree)//15]
            elif radius < 50:
                image = Img.n_opera[int(degree)//15]
            else :
                image = Img.b_opera[int(degree)//15]
        elif browser == "chrome":
            if radius < 30:
                image = Img.s_chrome[int(degree)//15]
            elif radius < 50:
                image = Img.n_chrome[int(degree)//15]
            else :
                image = Img.b_chrome[int(degree)//15]
        elif browser == "safari":
            if radius < 30:
                image = Img.s_safari[int(degree)//15]
            elif radius < 50:
                image = Img.n_safari[int(degree)//15]
            else :
                image = Img.b_safari[int(degree)//15]
        elif browser == "firefox":
            if radius < 30:
                image = Img.s_firefox[int(degree)//15]
            elif radius < 50:
                image = Img.n_firefox[int(degree)//15]
            else :
                image = Img.b_firefox[int(degree)//15]
        elif browser == "ie":
            if radius < 30:
                image = Img.s_ie[int(degree)//15]
            elif radius < 50:
                image = Img.n_ie[int(degree)//15]
            else :
                image = Img.b_ie[int(degree)//15]
        return image

    def do_action(self):
        self.reset_group()

        for i in range(self.player_num):
            self.set_player(self.p[i])
        
        for i in range(self.died_num):
            self.set_died(self.died[i])
        #set bound
        self.set_wall()        
        
        #set card
        self.set_card()

        #set ball
        self.set_ball()
        
        #
        self.animation += 1
        if self.animation == 48:
            self.animation = 0        
        #

        self.env["uic"].add_event(Event.ui_event.EventDrawGround(self.env, self.priority + EVENT_CON.TICKS_PER_TURN, self.animation))
        '''
        if self.animation == 10:
            sss=Skill.EventSkill(self.env,0)
            sss.do_action()
        '''
        
class EventCloseWindow(BaseEvent):
    def __init__(self, env, pp):
        BaseEvent.__init__(self)
        self.env = env
        self.priority = pp
    def do_action(self):
        #pygame.event.pump()
        if self.env["pyQUIT"] or self.env["pyESC"]:
            self.env["uic"].stop()
            self.env["gamec"].stop()
        self.env["uic"].add_event(Event.ui_event.EventCloseWindow(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
