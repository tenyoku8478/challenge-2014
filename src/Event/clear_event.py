import pygame 

from Event.base_event import BaseEvent

from pygame.locals import *
import Event
import Config.event_config as EVENT_CON
import Event.ui_event as ui
import Event.scoreboard_event as score
import Event.timer_event as timer
import Tools.Img as Img

class EventInitClear(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
    def do_action(self):
        self.env["uic"].add_event(EventDrawClear(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
class EventDrawClear(BaseEvent):
    def __init__(self, env, ms):
        self.env = env
        self.priority = ms
        self.surf = env["screen"]
    def do_action(self):
        ui.p_group.clear(self.surf, Img.Background)
        ui.eye_group.clear(self.surf, Img.Background)
        ui.wall_group.clear(self.surf, Img.Background)
        for pp in (ui.muti_card):
            pp.clear(self.surf, Img.Background)
        #ui.card_group.clear(self.surf, Img.Background)
        ui.ball_group.clear(self.surf, Img.Background)
        ui.bound_group.clear(self.surf, Img.Background)
        ui.died_group.clear(self.surf, Img.Background)
        score.all_group.clear(self.surf, Img.Background)
        timer.all_group.clear(self.surf, Img.Background)


        ui.bound_group.draw(self.surf)
        ui.wall_group.draw(self.surf)
        for pp in (ui.muti_card):
            pp.draw(self.surf)
        #ui.card_group.draw(self.surf)
        ui.ball_group.draw(self.surf)
        ui.died_group.draw(self.surf)
        ui.p_group.draw(self.surf)
        ui.eye_group.draw(self.surf)
        score.all_group.draw(self.surf)
        timer.all_group.draw(self.surf)

        pygame.display.update()
        self.env["uic"].add_event(EventDrawClear(self.env, self.priority + EVENT_CON.TICKS_PER_TURN))
