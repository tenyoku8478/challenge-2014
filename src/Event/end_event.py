from sys import exit

import pygame
import pygame.font
from pygame.locals import *

from Event.base_event import BaseEvent
import Event.timer_event
import Event.scoreboard_event as SB_EVENT

import Config.event_config as EVENT_CON

font = pygame.font.Font('screen/FONT01.TTF', 78)
nclr = (97,66,125)

opera = pygame.image.load("screen/icon/opera.png")
firefox = pygame.image.load("screen/icon/firefox.png")
chrome = pygame.image.load("screen/icon/chrome.png")
safari = pygame.image.load("screen/icon/safari.png")
IE = pygame.image.load("screen/icon/IE.png")
browser_alpha = int(255*0.85)
opera.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
firefox.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
chrome.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
safari.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)
IE.fill((255, 255, 255, browser_alpha), None, pygame.BLEND_RGBA_MULT)

num = []
fname = "screen/num/0-1.png"
name_alpha = int (255*0.8)
for j in range(2) :
    fname=fname[0:13]+str(j+1)+fname[14:];
    for i in range(10) :
        fname=fname[0:11]+str(i)+fname[12:];
        tmp=pygame.image.load(fname)
        tmp=pygame.transform.smoothscale(tmp,(65,90))
        tmp.fill((255, 255, 255, name_alpha), None, pygame.BLEND_RGBA_MULT)
        num.append(tmp)
crown_alpha = int(255*0.85)
blank = pygame.image.load("screen/end/blank.png")
crown = pygame.image.load("screen/end/crown.png")
crown.fill((255, 255, 255, crown_alpha), None, pygame.BLEND_RGBA_MULT)
timesup = pygame.image.load("screen/end/timesup.png")
timer = Event.timer_event.all_group

background = pygame.image.load("screen/bg-light.jpg")
class timesup_sprite(pygame.sprite.Sprite) :
    def __init__(self,x,y,m) :
        pygame.sprite.Sprite.__init__(self)
        self.image = m
        self.rect=self.image.get_rect()
        self.x = x
        self.y = y
        self.dy = 0
        self.rect.topleft=(x,y)
    def change(self,m) :
        self.image = m
        self.y += 1
        self.rect.topleft=(self.x,self.y)
    def down(self,m) :
        self.image = m
        self.dy+= 112/42
        while self.dy>=1 :
            self.dy-= 1
            self.y += 1
        self.rect.topleft=(self.x,self.y)

tu_sprite = timesup_sprite(155,48,timesup)
all_group = pygame.sprite.Group()
all_group.add(tu_sprite)
class EventEnd(BaseEvent) :

    def __init__(self,env,ms):
        self.env = env
        self.priority = ms
    def do_action(self) :
        ed = pygame.mixer.Sound('sounds/ed.wav')
        beat = pygame.mixer.Sound('sounds/end_beat.wav')
        cheer = pygame.mixer.Sound('sounds/cheer.wav')
        self.screen = self.env["screen"]
        self.final = []
        for i in range(4) :
            img = blank.copy()
            #for w in range(img.get_width()) :
            #    for h in range(img.get_height()) :
            #        img.set_at((w,h),(255,255,255,0))
            #img.fill((255, 255, 255, 0), None, pygame.BLEND_RGBA_MULT)
            #tmp.set_alpha(0)
            self.final.append(img)
        for i in range(4) :    
            self.final[i].blit(font.render(self.env["player"][i].name, 1, nclr),(20,0))

        for i in range(4) :        
            if self.env["player"][i].browser=="ie" :
                self.final[i].blit(IE,(60,110))
            elif self.env["player"][i].browser=="safari" :
                self.final[i].blit(safari,(60,90))
            elif self.env["player"][i].browser=="chrome" :
                self.final[i].blit(chrome,(60,110))
            elif self.env["player"][i].browser=="firefox" :
                self.final[i].blit(firefox,(60,110))
            elif self.env["player"][i].browser=="opera" :
                self.final[i].blit(opera,(60,110))
        ss = []
        for j in range(4) :
            dig = 10000
            score = self.env["player"][j].score
            ss.append((j,score))
            for i in range(5) :
                if i %2 ==0 :
                    ten=10
                else :
                    ten=0
                t = score//dig
                score%=dig
                dig//=10
                self.final[j].blit(num[t+ten],(60*i,330))
        list.sort(ss)
        for i in range(3,-1,-1) :
            for j in range(0,i) :
                if ss[j][1] > ss[j+1][1] :
                    tp = ss[j]
                    ss[j] = ss[j+1]
                    ss[j+1] = tp
        self.rank = []
        for i in range(4):
            self.rank.append(ss[i][0])
            
    
        t = self.screen.copy()
        for i in range(42) :
            if i <32 :
                t.set_alpha(255-i*6)
                self.screen.blit(background,(0,0))
                self.screen.blit(t,(0,0))
                timer.draw(self.screen)
                tt = self.screen.copy()
            tmp = timesup.copy()
            tmp.fill((255, 255, 255, i*6), None, pygame.BLEND_RGBA_MULT)
            tu_sprite.change(tmp)
            all_group.draw(self.screen)
            pygame.display.update()
            all_group.clear(self.screen,tt)

        ed.play()

        pygame.time.wait(2000)
        for i in range(42) :
            if i <32 :
                t.set_alpha(62-i*2)
                self.screen.blit(background,(0,0))
                self.screen.blit(t,(0,0))
                tt = self.screen.copy()
            tmp = timesup.copy()
            tmp.fill((255, 255, 255, 246-i*6), None, pygame.BLEND_RGBA_MULT)
            tu_sprite.down(tmp)
            all_group.draw(self.screen)
            pygame.display.update()
            all_group.clear(self.screen,tt)
        pygame.time.wait(1000)
        self.final[self.rank[0]]=pygame.transform.smoothscale(self.final[self.rank[0]],(120,150))
        self.screen.blit(self.final[self.rank[0]],(26,75))
        pygame.display.update()
        beat.play()
        pygame.time.wait(1000)
        self.final[self.rank[1]]=pygame.transform.smoothscale(self.final[self.rank[1]],(160,200))
        self.screen.blit(self.final[self.rank[1]],(750,63))
        pygame.display.update()
        beat.play()
        pygame.time.wait(1000)
        self.final[self.rank[2]]=pygame.transform.smoothscale(self.final[self.rank[2]],(280,350))
        self.screen.blit(self.final[self.rank[2]],(150,95))
        pygame.display.update()
        beat.play()
        pygame.time.wait(1000)
        self.screen.blit(self.final[self.rank[3]],(380,105))
        self.screen.blit(crown,(422,168))
        pygame.display.update()
        beat.play()
        cheer.play()
        self.env['uic'].stop()

#import Event.end_event
#self.env["uic"].add_event(Event.end_event.EventEnd(self.env, 1000))
