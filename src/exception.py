class AIReturnValueError(Exception):
    def __init__(self, name, msg):
        self.msg = msg
        self.name = name

    def __str__(self):
        return self.msg

class SkillExcessError(Exception):
    def __init__(self, name, skillname):
        self.skillname = skillname
        self.name = name

    def __str__(self):
        return "使用過多次數的[" + self.skillname + "]"
