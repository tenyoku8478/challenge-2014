ROOM_WIDTH = 450
ROOM_HEIGHT = 450
ROOM_LEFT = 85
ROOM_TOP = 45
ROOM_RADIUS = ROOM_WIDTH / 2
ROOM_CTR = ( ROOM_LEFT + ROOM_RADIUS, ROOM_TOP + ROOM_RADIUS )

PLAYER_MASS = 2
PLAYER_MASSES = [2, 4, 6]
PLAYER_SIZES = [50, 70, 125]
PLAYER_OMEGA = 1.5


PLAYER_ACCS = [150, 300, 450]
PLAYER_NORMAL_ACC = 300
PLAYER_IE_ACC = 100

START_DRAG = 1
COLLISION_DRAG = 2
TERMINATE_SPEED = 50
C = 200

BLACKHOLE = 500000

EPS = 1e-6

BALL_MASS = 1
BALL_SIZE = 50
BALL_SPEED = 300

BACHI = 3000000
BACHI_LOCK = 5
