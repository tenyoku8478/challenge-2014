import math
class Vector:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def dis(p1, p2):
        return math.sqrt((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)

    def get_theta(self):
        if self.x == 0:
            if self.y > 0:
                theta = math.pi / 2
            else:
                theta = -math.pi / 2
        else:
            print(type(self.x))
            print(type(self.y))
            theta = math.atan(self.y / self.x)
            if self.x < 0:
                theta += math.pi
        if theta < 0:
            theta += math.pi * 2
        return theta

    def dot(self, v):
        return self.x * v.x + self.y * v.y

    def len(self):
        return math.sqrt(self.x * self.x + self.y * self.y)

    def __add__(self, v):
        return Vector(self.x + v.x, self.y + v.y)

    def __mul__(self, c):
        return Vector(self.x * c, self.y * c)

    def __truediv__(self, c):
        return Vector(self.x / c, self.y / c)

    def get_degree(self):
        import math
        arc = math.atan2(self.y, self.x)
        if arc < 0:
            arc += 2*math.pi
        return arc
