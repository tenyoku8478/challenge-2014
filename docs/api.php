<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://yandex.st/highlightjs/8.0/highlight.min.js"></script>
    <link rel="stylesheet" href="style.css"></link>
    <link rel="stylesheet" href="http://yandex.st/highlightjs/8.0/styles/vs.min.css">
    <script>
        $(function(){
            $('h1').after('<button id="toggle">隱藏/開啟範例code</button>');
            $('#toggle').on('click', function(){
                $('h3:contains("Example"), pre:has(code)').toggle();
            });
            $('pre code').addClass('python');
            hljs.initHighlightingOnLoad();
        })
    </script>
    <style>
        pre code {
            font-size: 12pt;
        }
    </style>
</head>
<body>
<?php include('api_body.html') ?>
</body>
</html>
