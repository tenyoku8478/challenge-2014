# 2014 臺大椰林資訊營 AI Challenge - API使用說明
## 遊戲方式：
在 Challenge 資料夾下開啟 cmd ，並輸入：
```
python3 main.py
```
即可開始遊戲！（四隊都是手動控制，用鍵盤左上方的數字鍵 1 - 4 來操控）  
輸入
```
python3 main.py 101 102
```
即可開啟 team101 與 team102 的 AI 進行遊戲，不足的部份以手動控制補齊（用 3 & 4 來操控）

## AI 基礎
* 所有的 AI 程式碼都必須寫在 AI 這個資料夾中，並以自己小隊的編號命名為 "teamX.py" ， X 為小隊編號（ 1 - 10 ）
* 其中已經包含了許多簡單的 AI 程式碼，包括：
    * team0.py 是 AI 的模板，可以直接複製一份開始設計屬於自己的 AI ！
    * team101.py - team104.py 是 Hunter 所提供的範例 AI ，可以參考裡面的程式碼來獲得靈感！
    * team111.py - team777.py 分別對應到 7 種不同特殊技能，可以開來看看放招的效果

## helper —— 取得場上資訊
helper 裏面有大量函式幫助你獲得目前場上的資訊！

※ theta 單位為弧度，介於 0 ~ 2π ，以向右為 0 度繞順時針方向計算  
※ x 向右為正， y 向下為正，單位為 pixel （像素）  
※ pos 為 (x, y) 的 tuple  

## 場地info
* helper.get_field_radius(): 獲得場地半徑( 回傳常數 )
* helper.get_walls(): 獲得現在出現的牆(回傳list)，每個牆用一個tuple (theta1, theta2) 表示牆的範圍
* helper.get_remain_time(): 獲得現在的剩餘時間( 回傳常數 )
* helper.get_cards(): 獲得場上所有卡片的位置(回傳list)，每個卡片用一個tuple (x, y) 表示卡片的位置
* helper.get_chance(): 獲得場上機會（！）卡片的位置(回傳list)，每個卡片用一個tuple (x, y) 表示機會卡片的位置
* helper.get_destiny(): 獲得場上命運（？）卡片的位置(回傳list)，每個卡片用一個tuple (x, y) 表示命運卡片的位置
* helper.cal_vector(pos): 將 pos (x, y) 轉換成極座標 (回傳(r, theta) )
* helper.check_degree_eq(theta1, theta2): 確認兩個角度是否相等 (回˙傳True或False)
* helper.check_degree_eq(theta1, theta2, deviation)

### Example
```
r = helper.get_field_radius()
print(r) #輸出場地半徑

walls = helper.get_walls() #獲得現在出現的牆
#由list第一筆資料到最後一筆資料，並輸出所有牆的角度範圍
for wall in walls:
    theta1 = wall[0]
    theta2 = wall[1] 
    print(theta1, theta2) #輸出theta1,theta2，表示在角度範圍內有牆
    
time = helper.get_remain_time()
print(time) #輸出剩餘時間

cards = helper.get_cards() #獲得場上所有卡片的位置
#由list第一筆資料到最後一筆資料，並輸出所有卡片的位置
for card in cards:
    x = card[0]
    y = card[1]
    print(x, y)#輸出卡片x, y位置
    
chances = helper.get_chance() #獲得場上所有機會卡片的位置
#由list第一筆資料到最後一筆資料，並輸出所有機會卡片的位置
for chance in chances:
    x = chance[0]
    y = chance[1]
    print(x, y)#輸出卡片x, y位置
    
destines = helper.get_destiny() #獲得場上所有命運卡片的位置
#由list第一筆資料到最後一筆資料，並輸出所有命運卡片的位置
for destiny in destines:
    x = destiny[0]
    y = destiny[1]
    print(x, y)#輸出卡片x, y位置
    
pos = (1, 1)#設定pos位於(1,1)
r, theta = helper.cal_vector(pos) #換算成極座標
print(r, theta)#輸出pos距離圓心長度與角度

theta1 = 0, theta2 = 2π #設定theta1, theta2
#輸出兩個角度是否相同
if helper.check_degree_eq( theta1, theta2):
    print("Same")
else:
    print("Different")
    
#0.4表示角度誤差值,若沒有給誤差值,預設為0.05（弧度）   
if helper.check_degree_eq( theta1, theta2, 0.4): 
    print("Same")
else:
    print("Different")
```

## 自己info
helper.me 表示自己

### Example
```
player = helper.me #獲得自己資料
```

## 對手info
* helper.get_others_distance(): 獲得對手清單(list)，依照相對於自己的距離由近到遠排序
* helper.get_others_degree(): 獲得對手清單(list)，依照相對於自己延目前方向旋轉的角度由小到大排序
* helper.get_others_score(): 獲得對手清單(list)，依照分數由高到低排序

### Example
```
# 由距離長度獲得對手資料
player = helper.get_others_distance()[0] #獲得距離自己最近的玩家資料
player = helper.get_others_distance()[1] #獲得距離自己第二近的玩家資料
player = helper.get_others_distance()[2] #獲得距離自己最遠的玩家資料

# 由旋轉角度獲得對手資料
player = helper.get_others_degree()[0] #獲得自己延目前方向旋轉的角度最小的玩家資料
player = helper.get_others_degree()[1] #獲得自己延目前方向旋轉的角度第二小的玩家資料
player = helper.get_others_degree()[2] #獲得自己延目前方向旋轉的角度最大的玩家資料

# 由分數高低獲得對手資料 
player = helper.get_others_score()[0] #獲得(除了自己)分數最高的玩家資料
player = helper.get_others_score()[1] #獲得(除了自己)分數第二高的玩家資料
player = helper.get_others_score()[2] #獲得(除了自己)分數最低的玩家資料
```

## 玩家資料使用說明
※ player 為任意玩家的資料（可以是自己也可以是別人）

* player.is_dead(): player是否已死亡 (回傳True或False)
* player.is_invisible(): player是否隱形 (回傳True或False)
* player.is_lock(): player是否處於暈眩(><)狀態 (回傳True或False)

### Example
```
if player.is_dead():
    print("player is dead.")
else:
    print("player is alive.")
```
※注意！若對手為隱形狀態同樣會被視為死亡，這時候呼叫以下函式所得到的值都是沒有意義（可能對也可能錯）的。

* player.get_position(): 獲得player相對於圓心的座標位置 ( 回傳(x, y) )

### Example
```
#方法一
x, y = player.get_position()
print(x, y) # 輸出player的x, y座標

#方法二
pos = player.get_position()
print(pos[0], pos[1]) # 輸出player的x, y座標
```

* player.get_position_r(): 獲得player相對於圓心的極座標位置 ( 回傳(r, theta) )
* player.get_speed(): 獲得player的速度與方向 ( 回傳(v, theta) )

### Example
```
#方法一
r, theta = player.get_position_r()
print(r, theta) # 輸出player的半徑與角度
v, theta = player.get_speed()
print(v, theta) # 輸出player的速度量值與方向角度

#方法二
vector = player.get_position_r()
print(vector[0], vector[1]) # 輸出player的半徑與角度
vector = player.get_speed()
print(vector[0], vector[1]) # 輸出player的速度量值與方向角度
```

* player.get_degree(): 獲得player現在眼睛面對的方向 ( 回傳theta )
* player.get_rotate_dir(): 獲得player現在旋轉的方向 ( 回傳1或-1，1代表順時針，-1逆時針 )
* player.get_size(): 獲得player現在的半徑( 回傳常數 )
* player.get_mass(): 獲得player現在的質量( 回傳常數 )
* player.get_score(): 獲得player現在的分數( 回傳常數 )

### Example
```
theta = player.get_degree()
print(theta) #輸出player面對的角度

rotate = player.get_rotate_dir()
print(rotate) #輸出1或-1, 1代表順時針轉，-1逆時針轉

r = player.get_size()
print(r) #輸出player的半徑

mass = player.get_mass()
print(mass) #輸出player的質量

score = player.get_score()
print(score) #輸出player的分數
```

* player.cal_rel_degree(pos): 計算根據現在的旋轉方向，計算還要轉多少弧度才會轉到 pos 這個點 ( 回傳(theta) )
* player.cal_vector(pos): 計算 pos 相對於player的位置 ( 回傳(r, theta) )
* player.check_facing(pos) ( 回傳True或 False )
* player.check_facing(pos, deviation): 測試player是否面對 pos 這個點 ( 回傳True或 False l)，可指定誤差容許範圍deviation（單位為弧度）

### Example
```
#方法一
theta = player.cal_rel_degree( (0,0) )
print(theta) #輸出player轉多少弧度才會轉到圓心theta角度

r, theta = player.cal_vector( (0,0) )
print(r, theta) #輸出player相對於離心的距離與旋轉角度

if player.check_facing( (0,0) ): #輸出是否面對(0,0)位置
    print("player is facing it")
else:
    print("Not facing it")
    
#0.4表示角度誤差值,若沒有給誤差值,預設為0.05   
if player.check_facing( (0,0), 0.4):
    print("player is facing it")
else:
    print("Not facing it")
    
#方法二
pos = (0,0)
theta = player.cal_rel_degree(pos)
print(theta) #輸出player還要轉多少弧度才會轉到場地圓心

r, theta = player.cal_vector(pos)
print(r, theta) #輸出場地原心相對於自己的距離與角度

if player.check_facing(pos): #輸出是否面對pos位置
    print("I am facing it")
else:
    print("Not facing it")
    
#0.4表示角度誤差值,若沒有給誤差值,預設為0.05（弧度）    
if player.check_facing( pos, 0.4):
    print("I am facing it")
else:
    print("Not facing it")
```

## AI 回傳值：
AI回傳值分兩類，一種是按鍵型回傳值，另一種是SKILL型回傳值。

### 按鍵型回傳值：
* ActionNull: 維持前一個狀態
* ActionUnPress: 放開按鍵
* ActionPress: 按下按鍵

### SKILL型回傳值：
* ActionSkillSwap: 發動技能「來 你來 來」
    * 效果： 跟一隻角色互換位置
    * 使用方法： `return ActionSkillSwap, player # 跟 player 互換位置`

* ActionSkillScoreDouble: 發動技能「加倍奉還」
    * 效果： 時間內自己的分數無論加減都加倍
    * 持續時間： 15秒
    * 使用方法： `return ActionSkillScoreDouble`

* ActionSkillInvisible: 發動技能「你看不到我」
    * 效果： 讓其他人的 AI 看不到自己
    * 持續時間： 15秒
    * 使用方法： `return ActionSkillInvisible`

* ActionSkillPower: 發動技能「霸王色霸氣」
    * 效果： 以自己為圓心讓大家向外移動一段距離並陷入昏迷狀態
    * 持續時間： 5秒（昏迷時間
    * 使用方法： `return ActionSkillPower`

* ActionSkillChaseBeat: 發動技能「一大堆香蕉」
    * 效果： 召喚大量IE攻擊其他人且 IE 打死的分數歸自己所有
    * 使用方法： `return ActionSkillChaseBeat`

* ActionSkillAbs: 發動技能「進擊的巨人」
    * 效果： 把自己變大變重，其他人變小變輕
    * 持續時間： 10秒
    * 使用方法： `return ActionSkillAbs`

* ActionSkillCold: 發動技能「你聽過lag嗎」
    * 效果： 把其他人變成IE，移動速度會變慢
    * 持續時間： 10秒
    * 使用方法： `return ActionSkillCold`
